package com.tngtech.confluence.plugins;

import static com.tngtech.confluence.plugins.ParamIncludeDelegate.DESCRIPTION_MACRO_NAME;

import java.util.Map;
import java.util.Stack;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.tngtech.confluence.plugins.process.MacroFilter;
import com.tngtech.confluence.plugins.process.ParameterDeclaration;
import com.tngtech.confluence.plugins.process.XhtmlMacroFilter;
import com.tngtech.confluence.plugins.view.XhtmlRenderHelper;
import com.tngtech.confluence.plugins.process.XhtmlPageHelper;
import com.tngtech.confluence.plugins.view.RenderHelper;

public class XhtmlParamIncludeMacro implements Macro {

    public static final String PARAMETER_INHERITANCE = "com.tngtech.confluence.plugins.paraminclude.param-include.parameterinheritance";
    public static final String RECURSION_STACK = "com.tngtech.confluence.plugins.paraminclude.param-include.recursionstack";

    private final XhtmlContent xhtmlUtils;

    private ParamIncludeDelegate delegate;

    public XhtmlParamIncludeMacro(PageManager pageManager, PermissionManager permissionManager,
            XhtmlContent xhtmlUtils) {
        this.delegate = new ParamIncludeDelegate(pageManager, permissionManager, new XhtmlPageHelper(), new I18N());
        this.xhtmlUtils = xhtmlUtils;
    }

    public XhtmlParamIncludeMacro(PageManager pageManager, PermissionManager permissionManager,
            XhtmlContent xhtmlUtils, I18N i18n) {
        this.delegate = new ParamIncludeDelegate(pageManager, permissionManager, new XhtmlPageHelper(), i18n);
        this.xhtmlUtils = xhtmlUtils;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) {
        RenderHelper renderHelper = new XhtmlRenderHelper(xhtmlUtils, context);
        MacroFilter descriptionFilter = new XhtmlMacroFilter(DESCRIPTION_MACRO_NAME, xhtmlUtils, context);
        Stack<String> recursionStack = fetchRecursionProtectionStack(context);
        Stack<Map<String, ParameterDeclaration>> inheritanceStack = fetchParameterInheritanceStack(context);
        return delegate.execute(parameters, body, getSpaceKey(context), descriptionFilter,
                recursionStack, inheritanceStack, renderHelper);
    }

    @SuppressWarnings("unchecked")
    private Stack<String> fetchRecursionProtectionStack(ConversionContext context) {
        if (!context.hasProperty(RECURSION_STACK)) {
            context.setProperty(RECURSION_STACK, new Stack<String>());
        }
        Object object = context.getProperty(RECURSION_STACK);
        if (object instanceof Stack) {
            return (Stack<String>) object;
        } else {
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private Stack<Map<String, ParameterDeclaration>> fetchParameterInheritanceStack(ConversionContext context) {
        if (!context.hasProperty(PARAMETER_INHERITANCE)) {
            context.setProperty(PARAMETER_INHERITANCE, new Stack<Map<String, ParameterDeclaration>>());
        }
        Object object = context.getProperty(PARAMETER_INHERITANCE);
        if (object instanceof Stack) {
            return (Stack<Map<String, ParameterDeclaration>>) object;
        } else {
            return null;
        }
    }

    private String getSpaceKey(ConversionContext context) {
        String key = context.getSpaceKey();
        if (key != null) {
            return key;
        } else {
            return "";
        }
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
