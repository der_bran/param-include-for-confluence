package com.tngtech.confluence.plugins.process;

import com.atlassian.confluence.pages.Page;
import com.tngtech.confluence.plugins.process.PageHelper;

public class XhtmlPageHelper implements PageHelper {

    @Override
    public String getBody(Page page) {
        return page.getBodyAsString();
    }

    @Override
    public void setBody(Page page, String body) {
        page.setBodyAsString(body);
    }
}
