package com.tngtech.confluence.plugins.process;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class StringHelper {
    public static boolean isEmptyString(String s) {
        if (s.isEmpty()) {
            return true;
        }
        for (char c : s.toCharArray()) {
            if (!isSpaceCharacter(c)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isSpaceCharacter(char c) {
        // Non-breaking spaces aren't whitespaces, confluence uses them => double-check needed
        return (Character.isWhitespace(c) || Character.isSpaceChar(c));
    }

    public static String escpapeSpecialChars(String sequence) {
        Map<Character, String> replacements = getHtmlEscapeReplacements();
        StringBuilder builder = new StringBuilder(sequence.length());
        for (char letter : sequence.toCharArray()) {
            if (replacements.containsKey(letter)) {
                builder.append(replacements.get(letter));
            } else {
                builder.append(letter);
            }
        }
        return builder.toString();
    }

    private static Map<Character, String> getHtmlEscapeReplacements() {
        Map<Character, String> replacements = new HashMap<Character, String>();
        replacements.put('&', "&amp;");
        replacements.put('"', "&quot;");
        replacements.put('\'', "&#039;");
        replacements.put('<', "&lt;");
        replacements.put('>', "&gt;");
        replacements.put('{', "\\{");
        replacements.put('}', "\\}");
        return replacements;
    }

    public static String join(String delimiter, List<? extends Object> list) {
        if (list.isEmpty()) {
            return "";
        }
        Iterator<? extends Object> iterator = list.iterator();
        StringBuilder builder = new StringBuilder();
        builder.append(iterator.next());
        while (iterator.hasNext()) {
            builder.append(delimiter).append(iterator.next());
        }
        return builder.toString();
    }

    public static String join(String delimiter, String assignation, Map<String, String> map) {
        if (map.isEmpty()) {
            return "";
        }
        Iterator<Entry<String, String>> iterator = map.entrySet().iterator();
        StringBuilder builder = new StringBuilder();
        appendEntryTo(builder, assignation, iterator.next());
        while (iterator.hasNext()) {
            builder.append(delimiter);
            appendEntryTo(builder, assignation, iterator.next());
        }
        return builder.toString();
    }

    private static void appendEntryTo(StringBuilder builder, String assignation, Entry<String, String> entry) {
        builder.append(entry.getKey()).append(assignation).append(entry.getValue());
    }
}
