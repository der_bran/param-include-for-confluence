package com.tngtech.confluence.plugins.process;

import com.atlassian.confluence.pages.Page;

public interface PageHelper {

    public String getBody(Page page);

    public void setBody(Page page, String body);

}