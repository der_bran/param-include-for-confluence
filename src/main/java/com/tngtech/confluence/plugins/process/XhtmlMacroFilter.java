package com.tngtech.confluence.plugins.process;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionReplacer;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.tngtech.confluence.plugins.process.MacroDeclaration;
import com.tngtech.confluence.plugins.process.MacroFilter;

public class XhtmlMacroFilter implements MacroFilter {

    private final XhtmlContent xhtmlUtils;
    private final ConversionContext context;

    private String macroName;
    private MacroDeclaration capturedMacro = null;

    public XhtmlMacroFilter(String macroName, XhtmlContent xhtmlUtils, ConversionContext context) {
        this.macroName = macroName;
        this.xhtmlUtils = xhtmlUtils;
        this.context = context;
    }

    @Override
    public String filter(String body) {
        try {
            capturedMacro = null;
            MacroDefinitionReplacer replacer = getDescriptionMacroReplacer();
            return xhtmlUtils.replaceMacroDefinitionsWithString(body, context, replacer);
        } catch (XhtmlException e) {
            return body;
        }
    }

    private MacroDefinitionReplacer getDescriptionMacroReplacer() {
        return new MacroDefinitionReplacer() {
            @Override
            public String replace(MacroDefinition macro) throws XhtmlException {
                if (getName().equals(macro.getName())) {
                    if (getCapture() == null) {
                        setCapture(transformMacro(macro));
                    }
                    return "";
                } else {
                    return getXtmlUtils().convertMacroDefinitionToStorage(macro, getContext());
                }
            }
        };
    }

    protected void setCapture(MacroDeclaration capturedMacro) {
        this.capturedMacro = capturedMacro;
    }

    protected String getName() {
        return macroName;
    }

    protected MacroDeclaration transformMacro(MacroDefinition macro) {
        return new MacroDeclaration(macro.getName(), macro.getBodyText(),
                macro.getDefaultParameterValue(), macro.getParameters());
    }

    protected XhtmlContent getXtmlUtils() {
        return xhtmlUtils;
    }

    protected ConversionContext getContext() {
        return context;
    }

    @Override
    public MacroDeclaration getCapture() {
        return capturedMacro;
    }

    @Override
    public boolean hasCapture() {
        return capturedMacro != null;
    }
}
