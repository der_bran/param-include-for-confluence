package com.tngtech.confluence.plugins.process;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.tngtech.confluence.plugins.process.Notification.NotificationType;

/**
 * Transforms the parameter declaration from the body of a macro into a map
 * with parameter names and values.
 */
public class ParameterListing {

	private String bodyContent;

	private Map<String, ParameterDeclaration> includeParameters = new HashMap<String, ParameterDeclaration>();
	private List<String> invalidDeclarations = new ArrayList<String>();

	public static ParameterListing parse(String bodyContent) {
		return new ParameterListing(bodyContent);
	}

	private ParameterListing(String bodyContent) {
		this.bodyContent = bodyContent;

		parseParameterDeclarations();
	}

	private void parseParameterDeclarations() {
		Pattern pattern = RegularExpression.DECLARATION_DELIMITER;
		String[] splittedParameters = pattern.split(bodyContent);

		List<String> parameterDeclarations = Arrays.asList(splittedParameters);

		for (String declaration : parameterDeclarations) {
		    if (!StringHelper.isEmptyString(declaration)) {
				parseDeclaration(declaration);
			}
		}
	}

	private void parseDeclaration(String declaration) {
		try {
			ParameterDeclaration parameter = ParameterDeclaration.parse(declaration);
			includeParameters.put(parameter.getName(), parameter);
		} catch (InvalidDeclarationException e) {
			invalidDeclarations.add(declaration);
		}
	}

	public Map<String, ParameterDeclaration> getIncludeParameters() {
		return includeParameters;
	}

    public List<Notification> getNotifications() {
        if (!invalidDeclarations.isEmpty()) {
            List<Notification> errors = new ArrayList<Notification>();
            errors.add(new Notification(NotificationType.INVALID_DECLARATION, invalidDeclarations));
            return errors;
        } else {
            return Collections.emptyList();
        }
    }
}
