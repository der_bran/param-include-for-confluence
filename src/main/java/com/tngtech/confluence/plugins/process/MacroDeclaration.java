package com.tngtech.confluence.plugins.process;

import java.util.Map;

public class MacroDeclaration {

    private final String name;
    private final String body;
    private final String defaultParameter;
    private final Map<String, String> parameters;

    public MacroDeclaration(String name, String body, String defaultParameter,
            Map<String, String> parameters) {
        this.name = name;
        this.body = body;
        this.defaultParameter = defaultParameter;
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public String getBody() {
        return body;
    }

    public String getDefaultParameter() {
        return defaultParameter;
    }
    public Map<String, String> getParameters() {
        return parameters;
    }
}
