package com.tngtech.confluence.plugins.process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Uses a Map of parameter names and values to replace parameter
 * place holders in a given Page or String.
 */
public class IncludeBatch {

    private final String missingParameterFormat;

	private Matcher placeholderMatcher;

	private Map<String, ParameterDeclaration> includeParameters = Collections.emptyMap();
	private Stack<Map<String, ParameterDeclaration>> inheritanceStack;

	private Set<String> missingParameters = new HashSet<String>();

	private String content = "";
	private StringBuilder processedPageContent = new StringBuilder();

	public IncludeBatch(Map<String, ParameterDeclaration> includeParameters, Stack<Map<String, ParameterDeclaration>> inheritanceStack, String highlightFormat) {
		this.includeParameters = includeParameters;
        this.inheritanceStack = inheritanceStack;
		this.missingParameterFormat = String.format(highlightFormat,
	            RegularExpression.PARAMETER_PLACEHOLDER_ENCLOSING + "%s" + RegularExpression.PARAMETER_PLACEHOLDER_ENCLOSING);
	}

	public String insertIn(String body) {
		this.content = body;
		initProcessing();
		processContent();
		return processedPageContent.toString();
	}

	private void initProcessing() {
		Pattern pattern = RegularExpression.PARAMETER_PLACEHOLDER;
		placeholderMatcher = pattern.matcher(content);
	}

	private void processContent() {
		int lastEndPosition = 0;
		boolean matchSuccessful = false;

		while (true) {
			matchSuccessful = placeholderMatcher.find(lastEndPosition);
			if (!matchSuccessful) {
				break;
			}
			lastEndPosition = processNextContentPart(lastEndPosition);
		}
		appendEndOfPage(lastEndPosition);
	}

	private int processNextContentPart(int lastEndPosition) {
		int startPosition = placeholderMatcher.start();
		int endPosition = placeholderMatcher.end();

		appendPartBetweenParameters(lastEndPosition, startPosition);
		appendProcessedParameter();

		return endPosition;
	}

	private void appendPartBetweenParameters(int endPreviousParameter, int startNextParameter) {
		String sectionWithOutParameter = content.substring(endPreviousParameter, startNextParameter);
		processedPageContent.append(sectionWithOutParameter);
	}

	private void appendProcessedParameter() {
		String parameterName = placeholderMatcher.group(1);
		String parameterValue = lookupParameter(parameterName);

		processedPageContent.append(parameterValue);
	}

	private void appendEndOfPage(int endPreviousParameter) {
		String sectionWithOutParameter = content.substring(endPreviousParameter, content.length());
		processedPageContent.append(sectionWithOutParameter);
	}

	private String lookupParameter(String name) {
		if (includeParameters.containsKey(name)) {
		    ParameterDeclaration parameter = includeParameters.get(name);
		    parameter.use();
			return StringHelper.escpapeSpecialChars(parameter.getValue());
		} else {
		    return lookupInheritedParamter(name);
		}
	}

	private String lookupInheritedParamter(String name) {
	    ParameterDeclaration parameter = getInheritedParamter(name);
	    if (parameter != null) {
            parameter.use();
            return StringHelper.escpapeSpecialChars(parameter.getValue());
        } else {
            missingParameters.add(name);
            return highlightParameter(name);
        }
	}

	private ParameterDeclaration getInheritedParamter(String name) {
        for (int i = inheritanceStack.size() - 1; i >= 0; i--) {
            ParameterDeclaration parameter = inheritanceStack.get(i).get(name);
            if (parameter != null) {
                return parameter;
            }
        }
        return null;
	}

	protected String highlightParameter(String parameter) {
        return String.format(missingParameterFormat, StringHelper.escpapeSpecialChars(parameter));
	}

    public List<Notification> getNotifications() {
        List<Notification> errors = new ArrayList<Notification>();
        if (!missingParameters.isEmpty()) {
            errors.add(new Notification(Notification.NotificationType.MISSING_PARAMETER, missingParameters));
        }
        Set<String> unusedParameters = getUnusedParameters();
        if (unusedParameters.size() > 0) {
            errors.add(new Notification(Notification.NotificationType.UNUSED_PARAMETER, unusedParameters));
        }
        return errors;
    }

	private Set<String> getUnusedParameters() {
        Set<String> unusedParameters = new HashSet<String>();
        for (ParameterDeclaration parameter : includeParameters.values()) {
            if (!parameter.isUsed()) {
                unusedParameters.add(parameter.getName());
            }
        }
        return unusedParameters;
    }
}
