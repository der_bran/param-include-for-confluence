package com.tngtech.confluence.plugins.process;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Takes one parameter declaration and extracts its name and value.
 */
public class ParameterDeclaration {

    String name;
    String value;
	boolean used = false;

	public static ParameterDeclaration parse(String declaration) throws InvalidDeclarationException {
		return new ParameterDeclaration(declaration);
	}

	protected static ParameterDeclaration create(String name, String value) {
        return new ParameterDeclaration(name, value);
    }

	private ParameterDeclaration(String declaration) throws InvalidDeclarationException {
	    processParameterDeclaration(unescapeDelimiter(declaration));
	}

    private ParameterDeclaration(String name, String value) {
        this.name = name;
        this.value = value;
    }

	private String unescapeDelimiter(String declaration) {
		Pattern pattern = RegularExpression.UNESCAPE_DELIMITER;
		Matcher matcher = pattern.matcher(declaration);
		return matcher.replaceAll(RegularExpression.DELIMITER_SEQUENCE);
	}

	private void processParameterDeclaration(String declaration) throws InvalidDeclarationException {
		Pattern pattern = RegularExpression.PARAMETER_DECLARATION;
		Matcher matcher = pattern.matcher(declaration);

		if (!matcher.matches()) {
			throw new InvalidDeclarationException();
		}
		name = extractValidString(matcher, 1);
		value = extractValidString(matcher, 2);
	}

   private String extractValidString(Matcher matcher, int groupId) {
        String subsequence = matcher.group(groupId);
        if (subsequence == null) {
            subsequence = "";
        }
        return subsequence;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public void use() {
        used = true;
    }

    public boolean isUsed() {
        return used;
    }

    @Override
    public String toString() {
        return value;
    }
}
