package com.tngtech.confluence.plugins.process;

import java.util.Map;


/**
 * Selects the page ident from page parameter or unnamed parameter
 * and splits the ident into space key and page name.
 */
public class PageIdent {

	private static final String pageIdentParameterName = "page";
	private static final char pageIdentDelimiter = ':';

	private String spaceKey;
	private String pageTitle;

	public PageIdent(Map<String, String> macroParameters, String defaultSpaceKey) throws NoPageParameterException {
		String ident = selectIdentFromPageOrUnnamedParameter(macroParameters);
		if (StringHelper.isEmptyString(ident)) {
		    throw new NoPageParameterException();
		}
		splitInSpaceKeyAndPageName(ident, defaultSpaceKey);
	}

	private String selectIdentFromPageOrUnnamedParameter(Map<String, String> macroParameters) {
		if (macroParameters.containsKey(pageIdentParameterName)) {
			return macroParameters.get(pageIdentParameterName);
		} else if (macroParameters.containsKey("")) {
			return macroParameters.get("");
		} else {
			return "";
		}
	}

	private void splitInSpaceKeyAndPageName(String ident, String defaultSpaceKey) {
		int firstColonPosition = ident.indexOf(pageIdentDelimiter);

		if (firstColonPosition == -1) {
			spaceKey = defaultSpaceKey;
			pageTitle = ident;
		} else {
			if (firstColonPosition != 0) {
				spaceKey = ident.substring(0, firstColonPosition);
			} else {
				spaceKey = defaultSpaceKey;
			}
			pageTitle = ident.substring(firstColonPosition + 1, ident.length());
		}
	}

	public String getSpaceKey() {
		return spaceKey;
	}

	public String getTitle() {
		return pageTitle;
	}
}
