package com.tngtech.confluence.plugins.process;

import java.util.regex.Pattern;

public class RegularExpression {

    // Matches: [unicode-letters, decimals, underscore]+
    private static final String parameterName = "[\\p{L}\\d_]+";

    public static final String PARAMETER_PLACEHOLDER_ENCLOSING = "$";

    private static final String parameterPlaceholder = Pattern.quote(PARAMETER_PLACEHOLDER_ENCLOSING) + "("
            + parameterName + ")" + Pattern.quote(PARAMETER_PLACEHOLDER_ENCLOSING);

    // Matches: [whitespace]* name [whitespace]* [=] [whitespace]* value [whitespace]*
    // Confluence inserts occasionally nonbreaking whitespace, therefore \u00A0 is needed
    private static final String parameterDeclarationPattern = "[\\s\\u00A0]*(" + parameterName
            + ")[\\s\\u00A0]*=[\\s\\u00A0]*((?s).*?)[\\s\\u00A0]*";

    // Most common solution to implement multiple backslashes for escaping special char
    // Even count of backslashes escape themselves, uneven escapes the delimiter too
	private static final String delimiterEscapeSequence = "(?<=(?<!\\\\)(\\\\\\\\){0,20})";

	public static final String DELIMITER_SEQUENCE = "|";

	public static final Pattern DECLARATION_DELIMITER = Pattern
			.compile(delimiterEscapeSequence + Pattern.quote(DELIMITER_SEQUENCE));

	public static final Pattern UNESCAPE_DELIMITER = Pattern
			.compile(delimiterEscapeSequence + "\\\\" + Pattern.quote(DELIMITER_SEQUENCE));

	public static final Pattern PARAMETER_PLACEHOLDER = Pattern
			.compile(parameterPlaceholder);

	public static final Pattern PARAMETER_DECLARATION = Pattern
			.compile(parameterDeclarationPattern);
}
