package com.tngtech.confluence.plugins.process;

import java.util.Collection;


public class Notification {

    public enum NotificationType {
        MISSING_PARAMETER("Non existent parameter"),
        UNUSED_PARAMETER("Unused parameter"),
        INVALID_DECLARATION("Invalid declaration");

        private String message;

        private NotificationType(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    private NotificationType type;
    private Collection<String> parameters;

    public Notification(NotificationType type, Collection<String> parameters) {
        this.type = type;
        this.parameters = parameters;
    }

    public NotificationType getType() {
        return type;
    }

    public String getMessage() {
        return type.getMessage();
    }

    public Collection<String> getParameters() {
        return parameters;
    }
}