package com.tngtech.confluence.plugins.process;

public interface MacroFilter {

    public String filter(String body);

    public MacroDeclaration getCapture();

    public boolean hasCapture();

}