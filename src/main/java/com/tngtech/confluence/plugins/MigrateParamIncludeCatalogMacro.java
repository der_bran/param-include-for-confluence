package com.tngtech.confluence.plugins;

import java.util.Map;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.tngtech.confluence.plugins.view.XhtmlRenderHelper;

public class MigrateParamIncludeCatalogMacro extends BaseMacro {

    @SuppressWarnings("rawtypes")
    @Override
    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException {
        return String.format("<p>" + XhtmlRenderHelper.getHighlightFormatStatic() + "</p>", I18N.getTextStatic(I18N.MIGRATION_NOTE));
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.ALL;
    }

    @Override
    public boolean hasBody() {
        return false;
    }
}
