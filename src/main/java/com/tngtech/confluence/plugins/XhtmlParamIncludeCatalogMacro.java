package com.tngtech.confluence.plugins;

import static com.tngtech.confluence.plugins.ParamIncludeDelegate.CATALOG_MACRO_NAME;
import static com.tngtech.confluence.plugins.ParamIncludeDelegate.DESCRIPTION_MACRO_NAME;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.tngtech.confluence.plugins.process.MacroFilter;
import com.tngtech.confluence.plugins.process.XhtmlMacroFilter;
import com.tngtech.confluence.plugins.view.XhtmlRenderHelper;
import com.tngtech.confluence.plugins.process.XhtmlPageHelper;
import com.tngtech.confluence.plugins.view.RenderHelper;

public class XhtmlParamIncludeCatalogMacro implements Macro {

    private final XhtmlContent xhtmlUtils;

    private ParamIncludeCatalogDelegate delegate;

    public XhtmlParamIncludeCatalogMacro(LabelManager labelManager, PermissionManager permissionManager,
            BootstrapManager bootstrapManager, XhtmlContent xhtmlUtils) {
        this.delegate = new ParamIncludeCatalogDelegate(labelManager, permissionManager,
                bootstrapManager, new XhtmlPageHelper(), new I18N());
        this.xhtmlUtils = xhtmlUtils;
    }

    protected XhtmlParamIncludeCatalogMacro(LabelManager labelManager,
            PermissionManager permissionManager, BootstrapManager bootstrapManager,
            XhtmlContent xhtmlUtils, I18N i18n) {
        this.delegate = new ParamIncludeCatalogDelegate(labelManager, permissionManager,
                bootstrapManager, new XhtmlPageHelper(), i18n);
        this.xhtmlUtils = xhtmlUtils;
    }

    @Override
    public String execute(Map<String, String> parameters, String body, final ConversionContext context) {
        RenderHelper renderHelper = new XhtmlRenderHelper(xhtmlUtils, context);
        MacroFilter descriptionFilter = new XhtmlMacroFilter(DESCRIPTION_MACRO_NAME, xhtmlUtils, context);
        MacroFilter catalogFilter = new XhtmlMacroFilter(CATALOG_MACRO_NAME, xhtmlUtils, context);

        return delegate.execute(descriptionFilter, catalogFilter, renderHelper);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
