package com.tngtech.confluence.plugins;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.tngtech.confluence.plugins.view.RenderHelper;
import com.tngtech.confluence.plugins.view.XhtmlRenderHelper;

public class XhtmlParamIncludeDescriptionMacro implements Macro {

    private final XhtmlContent xhtmlUtils;

    private ParamIncludeDescriptionDelegate delegate;

    public XhtmlParamIncludeDescriptionMacro(XhtmlContent xhtmlUtils) {
        this.delegate = new ParamIncludeDescriptionDelegate(new I18N());
        this.xhtmlUtils = xhtmlUtils;
    }

    public XhtmlParamIncludeDescriptionMacro(XhtmlContent xhtmlUtils, I18N i18n) {
        this.delegate = new ParamIncludeDescriptionDelegate(i18n);
        this.xhtmlUtils = xhtmlUtils;
    }

    @Override
    @RequiresFormat(Format.Storage) // will be processed with other output when rendered
    public String execute(Map<String, String> parameters, String body,
            final ConversionContext context) {
        RenderHelper renderHelper = new XhtmlRenderHelper(xhtmlUtils, context);
        return delegate.execute(body, renderHelper);
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
