package com.tngtech.confluence.plugins;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.tngtech.confluence.plugins.process.IncludeBatch;
import com.tngtech.confluence.plugins.process.MacroFilter;
import com.tngtech.confluence.plugins.process.NoPageParameterException;
import com.tngtech.confluence.plugins.process.Notification;
import com.tngtech.confluence.plugins.process.PageHelper;
import com.tngtech.confluence.plugins.process.PageIdent;
import com.tngtech.confluence.plugins.process.ParameterDeclaration;
import com.tngtech.confluence.plugins.process.ParameterListing;
import com.tngtech.confluence.plugins.process.StringHelper;
import com.tngtech.confluence.plugins.view.IncludeView;
import com.tngtech.confluence.plugins.view.Message.MessageType;
import com.tngtech.confluence.plugins.view.RenderHelper;
import com.tngtech.confluence.plugins.view.View;


/**
 * Includes a page within another page and replaces place holders with given
 * parameters.
 */
public class ParamIncludeDelegate {

    public static final String DESCRIPTION_MACRO_NAME = "param-include-description";
    public static final String CATALOG_MACRO_NAME = "param-include-catalog";

    private final PageManager pageManager;
    private final PermissionManager permissionManager;
    private final PageHelper pageHelper;
    private final I18N i18n;

    protected ParamIncludeDelegate(PageManager pageManager, PermissionManager permissionManager,
            PageHelper pageHelper, I18N i18n) {
        this.pageManager = pageManager;
        this.permissionManager = permissionManager;
        this.pageHelper = pageHelper;
        this.i18n = i18n;
    }

    public String execute(Map<String, String> parameters, String body, String defaultSpaceKey,
            MacroFilter descriptionFilter, Stack<String> recursionStack,
            Stack<Map<String, ParameterDeclaration>> inheritanceStack, RenderHelper renderHelper) {
        IncludeView view = new IncludeView(renderHelper);
        Page page = fetchIncludedPageFrom(parameters, defaultSpaceKey, view);
        if (page != null) {
            if (recursionStack.contains(page.getIdAsString())) {
                String pageTitle = StringHelper.escpapeSpecialChars(page.getTitle());
                view.appendMessage(MessageType.WARNING, i18n.getMessageTitle(I18N.INLCUDE_RECURSION), i18n.getMessageBody(I18N.INLCUDE_RECURSION, pageTitle));
            } else {
                recursionStack.push(page.getIdAsString());
                renderPageWithParametersIn(view, page, body, descriptionFilter, inheritanceStack, renderHelper);
                recursionStack.pop();
            }
        }
        return view.toString();
    }

    protected Page fetchIncludedPageFrom(Map<String, String> parameters, String defaultSpaceKey, View view) {
        try {
            PageIdent ident = new PageIdent(parameters, defaultSpaceKey);
            return loadPermittedPage(ident, view);
        } catch (NoPageParameterException e) {
            view.appendMessage(MessageType.WARNING, i18n.getMessageTitle(I18N.MISSING_IDENT), i18n.getMessageBody(I18N.MISSING_IDENT));
            return null;
        }
    }

    private Page loadPermittedPage(PageIdent ident, View view) {
        Page page = pageManager.getPage(ident.getSpaceKey(), ident.getTitle());
        if (page != null) {
            if (hasPermissionFor(page)) {
                return page;
            } else {
                view.appendMessage(MessageType.WARNING, i18n.getMessageTitle(I18N.INSUFFICIENT_PERMISSIONS), i18n.getMessageBody(I18N.INSUFFICIENT_PERMISSIONS));
            }
        } else {
            String pageTitle = StringHelper.escpapeSpecialChars(ident.getTitle());
            if (hasBlogPost(ident.getSpaceKey(), ident.getTitle())) {
                view.appendMessage(MessageType.WARNING, i18n.getMessageTitle(I18N.INCLUDED_BLOG), i18n.getMessageBody(I18N.INCLUDED_BLOG, pageTitle));
            } else {
                view.appendMessage(MessageType.WARNING, i18n.getMessageTitle(I18N.MISSING_PAGE), i18n.getMessageBody(I18N.MISSING_PAGE, pageTitle));
            }
        }
        return null;
    }

    private boolean hasPermissionFor(Page page) {
        return permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(),
                Permission.VIEW, page);
    }

    private boolean hasBlogPost(String spaceKey, String pageTitle) {
        BlogPost post = pageManager.findPreviousBlogPost(spaceKey, new Date());
        while (post != null) {
            if (pageTitle.equals(post.getTitle())) {
                return true;
            }
            post = pageManager.findPreviousBlogPost(post);
        }
        return false;
    }

    protected void renderPageWithParametersIn(IncludeView view, Page page, String content,
            MacroFilter description, Stack<Map<String, ParameterDeclaration>> inheritanceStack, RenderHelper renderHelper) {
        ParameterListing listing = ParameterListing.parse(content);
        showErrorsIn(view, listing.getNotifications());
        IncludeBatch batch = new IncludeBatch(listing.getIncludeParameters(), inheritanceStack, renderHelper.getHighlightFormat());
        String body = pageHelper.getBody(page);
        body = batch.insertIn(body);
        body = description.filter(body);
        body = makeAbsoluteAttachmentResourceIdentifiers(body, page);
        renderWithInheritatedParametersIn(view, listing, inheritanceStack, body);
        showErrorsIn(view, batch.getNotifications());
    }

// <from com.tngtech.confluence.plugins.includecontentbylabel.IncludeContentByLabel>
     String makeAbsoluteAttachmentResourceIdentifiers(String storageContent, ContentEntityObject ceo) {
        // see https://confluence.atlassian.com/display/DOC/Confluence+Storage+Format#ConfluenceStorageFormat-Resourceidentifiers

        String title = " ri:content-title=\"" + StringEscapeUtils.escapeXml(ceo.getTitle()) + "\"";

        if (ceo instanceof Page) {
            Page page = (Page) ceo;
            String reference = "<ri:page" + title
                    + " ri:space-key=\"" + page.getSpaceKey() + "\""
                    + " />";
            return makeAbsoluteAttachmentResourceIdentifiers(storageContent, reference);
        } else if (ceo instanceof BlogPost) {
            BlogPost blogPost = (BlogPost) ceo;
            String reference = "<ri:blog-post" + title
                    + " ri:space-key=\"" + blogPost.getSpaceKey() + "\""
                    + " ri:posting-day=\"" + blogPost.getPostingYear() + "/" + blogPost.getPostingMonthNumeric() + "/" + blogPost.getPostingDayOfMonth() + "\""
                    + " />";
            return makeAbsoluteAttachmentResourceIdentifiers(storageContent, reference);
        } else
            return storageContent;
    }

    private String makeAbsoluteAttachmentResourceIdentifiers(String storageContent, String reference) {
        StringBuffer stringBuffer = new StringBuffer();
        Pattern pattern = Pattern.compile("(<ri:attachment ri:filename=\"[^\"]*\") />");
        Matcher matcher = pattern.matcher(storageContent);
        while (matcher.find())
        {
            matcher.appendReplacement(stringBuffer, matcher.group(1));
            stringBuffer.append(">")
                        .append(reference)
                        .append("</ri:attachment>");
        }
        if(stringBuffer.length() > 0)
        {
            matcher.appendTail(stringBuffer);
            return stringBuffer.toString();
        }
        return storageContent;
    }
// </from com.tngtech.confluence.plugins.includecontentbylabel.IncludeContentByLabel>

    private void renderWithInheritatedParametersIn(IncludeView view, ParameterListing listing,
            Stack<Map<String, ParameterDeclaration>> inheritanceStack, String body) {
        inheritanceStack.push(listing.getIncludeParameters());
        view.renderContent(body);
        inheritanceStack.pop();
    }

    private void showErrorsIn(IncludeView view, List<Notification> errors) {
        for (Notification error : errors) {
            switch (error.getType()) {
            case INVALID_DECLARATION:
                view.insertMessage(MessageType.WARNING, i18n.getMessageTitle(I18N.PARSE_ERROR),
                        i18n.getMessageBody(I18N.PARSE_ERROR), i18n.getMessageLabel(I18N.PARSE_ERROR),
                        error.getParameters());
                break;
            case UNUSED_PARAMETER:
                view.insertMessage(MessageType.NOTE, i18n.getMessageTitle(I18N.UNUSED_PARAMETER),
                        i18n.getMessageBody(I18N.UNUSED_PARAMETER), i18n.getMessageLabel(I18N.UNUSED_PARAMETER), error.getParameters());
                break;
            case MISSING_PARAMETER:
                view.insertMessage(MessageType.WARNING, i18n.getMessageTitle(I18N.MISSING_PARAMETER),
                        i18n.getMessageBody(I18N.MISSING_PARAMETER), i18n.getMessageLabel(I18N.MISSING_PARAMETER), error.getParameters());
                break;
            default:
                view.insertMessage(MessageType.WARNING, i18n.getMessageTitle(I18N.UNKNOWN_ERROR),
                        i18n.getMessageBody(I18N.UNKNOWN_ERROR));
                break;
            }
        }
    }
}
