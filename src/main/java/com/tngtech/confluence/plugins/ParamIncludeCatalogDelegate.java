package com.tngtech.confluence.plugins;

import static com.atlassian.confluence.security.Permission.VIEW;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.tngtech.confluence.plugins.process.MacroFilter;
import com.tngtech.confluence.plugins.process.PageHelper;
import com.tngtech.confluence.plugins.process.StringHelper;
import com.tngtech.confluence.plugins.view.CatalogView;
import com.tngtech.confluence.plugins.view.Message.MessageType;
import com.tngtech.confluence.plugins.view.RenderHelper;

public class ParamIncludeCatalogDelegate {

    public static final String INCLUDE_LABEL = "include";
    public static final String EMPTY_BODY = "<p></p>";

    private final LabelManager labelManager;
    private final PermissionManager permissionManager;
    private final BootstrapManager bootstrapManager;
    private final PageHelper pageHelper;
    private final I18N i18n;

    protected ParamIncludeCatalogDelegate(LabelManager labelManager,
            PermissionManager permissionManager, BootstrapManager bootstrapManager,
            PageHelper pageHelper, I18N i18n) {
        this.labelManager = labelManager;
        this.permissionManager = permissionManager;
        this.bootstrapManager = bootstrapManager;
        this.pageHelper = pageHelper;
        this.i18n = i18n;
    }

    public String execute(MacroFilter descriptionFilter, MacroFilter catalogFilter, RenderHelper renderHelper) {
        CatalogView view = new CatalogView(renderHelper);
        renderPermittedPagesIn(view, descriptionFilter, catalogFilter);
        return view.toString();
    }

    private void renderPermittedPagesIn(CatalogView view, MacroFilter descriptionFilter,
            MacroFilter catalogFilter) {
        List<Page> pages = getPermittedLabeledPages();
        if (pages.size() > 0) {
            for (Page page : sortBySpaceAndTitle(pages)) {
                renderFilteredIncludePageIn(view, page, descriptionFilter, catalogFilter);
            }
        } else {
            view.appendMessage(MessageType.NOTE, i18n.getMessageTitle(I18N.EMPTY_CATALOG), i18n.getMessageBody(I18N.EMPTY_CATALOG));
        }
    }

    private void renderFilteredIncludePageIn(CatalogView view, Page page, MacroFilter descriptionFilter,
            MacroFilter catalogFilter) {
        String body = pageHelper.getBody(page);
        body = descriptionFilter.filter(body);
        body = catalogFilter.filter(body);
        if (catalogFilter.hasCapture()) {
            String pageTitle = StringHelper.escpapeSpecialChars(page.getTitle());
            view.appendMessage(MessageType.NOTE, i18n.getMessageTitle(I18N.INCLUDED_CATALOG), i18n.getMessageBody(I18N.INCLUDED_CATALOG, pageTitle));
        }
        view.appendPageUrlBox(page, bootstrapManager.getWebAppContextPath());
        if (descriptionFilter.hasCapture()) {
            String text = descriptionFilter.getCapture().getBody();
            view.appendMessage(MessageType.INFO, i18n.getText(I18N.DESCRIPTION_TITLE), text);
        }
        view.appendPanel(body);
        view.appendVerticalSpace();
    }

    private List<Page> getPermittedLabeledPages() {
        int unlimitedResults = -1;
        Label label = new Label(INCLUDE_LABEL);
        List<ContentEntityObject> entities = labelManager.getContentForLabel(0, unlimitedResults,label).getList();
        List<Page> pages = new ArrayList<Page>();
        for (ContentEntityObject entity : entities) {
            if (isPermittedExistingPage(entity)) {
                pages.add((Page) entity);
            }
        }
        return pages;
    }

    private boolean isPermittedExistingPage(ContentEntityObject entity) {
        if (entity.isDeleted()) {
            return false;
        }
        if (entity.getTypeEnum() != ContentTypeEnum.PAGE) {
            return false;
        }
        Page page = (Page) entity;
        if (!hasPermissionFor(page)) {
            return false;
        }
        return true;
    }

    private boolean hasPermissionFor(Page page) {
        return permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), VIEW, page);
    }

    private List<Page> sortBySpaceAndTitle(List<Page> pages) {
        List<Page> sort = new ArrayList<Page>(pages);
        Collections.sort(sort, createPageComperator());
        return sort;
    }

    private Comparator<Page> createPageComperator() {
        return new Comparator<Page>() {
            @Override
            public int compare(Page page1, Page page2) {
                int compareSpaceKey = page1.getSpaceKey().compareTo(page2.getSpaceKey());
                if (compareSpaceKey != 0) {
                    return compareSpaceKey;
                } else {
                    return page1.getTitle().compareTo(page2.getTitle());
                }
            }
        };
    }
}
