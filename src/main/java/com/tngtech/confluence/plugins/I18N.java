package com.tngtech.confluence.plugins;

import com.atlassian.confluence.core.ConfluenceActionSupport;


public class I18N {
    public static final String MISSING_PAGE = "com.tngtech.confluence.plugins.paraminclude.error.missingpage";
    public static final String INSUFFICIENT_PERMISSIONS = "com.tngtech.confluence.plugins.paraminclude.error.insufficientpermissions";
    public static final String PARSE_ERROR = "com.tngtech.confluence.plugins.paraminclude.error.parseerror";
    public static final String UNUSED_PARAMETER = "com.tngtech.confluence.plugins.paraminclude.error.unusedparameter";
    public static final String MISSING_PARAMETER = "com.tngtech.confluence.plugins.paraminclude.error.missingparameter";
    public static final String MISSING_IDENT = "com.tngtech.confluence.plugins.paraminclude.error.missingident";
    public static final String INCLUDED_BLOG = "com.tngtech.confluence.plugins.paraminclude.error.includedblog";
    public static final String INCLUDED_CATALOG = "com.tngtech.confluence.plugins.paraminclude.error.includedcatalog";
    public static final String INLCUDE_RECURSION = "com.tngtech.confluence.plugins.paraminclude.error.includerecursion";
    public static final String EMPTY_CATALOG = "com.tngtech.confluence.plugins.paraminclude.error.emptycatalog";
    public static final String UNKNOWN_ERROR = "com.tngtech.confluence.plugins.paraminclude.error.unknownerror";

    public static final String DESCRIPTION_TITLE = "com.tngtech.confluence.plugins.paraminclude.label.descriptiontitle";
    public static final String MIGRATION_NOTE = "com.tngtech.confluence.plugins.paraminclude.label.migrationnote";

    public String getMessageTitle(String key) {
        return getText(key + ".title");
    }

    public String getMessageBody(String key) {
        return getText(key + ".body");
    }

    public String getMessageBody(String key, Object... list) {
        return getText(key + ".body", list);
    }

    public String getMessageLabel(String key) {
        return getText(key + ".label");
    }

    public String getText(String key) {
        return ConfluenceActionSupport.getTextStatic(key);
    }

    public static String getTextStatic(String key) {
        return ConfluenceActionSupport.getTextStatic(key);
    }

    private String getText(String key, Object... list) {
        return ConfluenceActionSupport.getTextStatic(key, list);
    }
}
