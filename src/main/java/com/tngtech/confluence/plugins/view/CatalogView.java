package com.tngtech.confluence.plugins.view;

import com.atlassian.confluence.pages.Page;
import com.tngtech.confluence.plugins.view.Message.MessageType;

public class CatalogView extends View {

    private static final String verticalSpace = "<p>&nbsp;</p>";

    public CatalogView(RenderHelper elementHelper) {
        super(elementHelper);
    }

    public void appendPageUrlBox(Page page, String baseUrl) {
        Message url = new Message(MessageType.INFO, renderHelper);
        url.setBody(renderHelper.getPageLink(page, baseUrl + page.getUrlPath()));
        url.hideIcon();
        append(url);
    }

    public void appendPanel(String content) {
        Panel panel = new Panel(renderHelper);
        panel.setContent(content);
        append(panel);
    }

    public void appendVerticalSpace() {
        append(verticalSpace);
    }
}
