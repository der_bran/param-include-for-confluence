package com.tngtech.confluence.plugins.view;

import java.util.HashMap;
import java.util.Map;

import com.tngtech.confluence.plugins.process.MacroDeclaration;

public class Panel extends AbstractElement {

    public Panel(RenderHelper renderHelper) {
        super(renderHelper);
    }

    private String content = "";

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public MacroDeclaration getMacro() {
        Map<String, String> macroParameter = new HashMap<String, String>();
        macroParameter.put("bgColor", "transparent");
        return new MacroDeclaration("panel", content, "", macroParameter);
    }

    @Override
    public String getFallback() {
        return content;
    }
}
