package com.tngtech.confluence.plugins.view;

import java.util.Collection;

import com.tngtech.confluence.plugins.view.Message.MessageType;

public class IncludeView extends View {

    public IncludeView(RenderHelper elementHelper) {
        super(elementHelper);
    }

    public void insertMessage(MessageType type, String title, String body, String label, Collection<String> parameters) {
        ParameterMessage message = new ParameterMessage(type, renderHelper);
        message.setTitle(title);
        message.setBody(body);
        message.setParameters(parameters, label);
        insert(message);
    }
}
