package com.tngtech.confluence.plugins.view;

import com.tngtech.confluence.plugins.process.MacroDeclaration;

public abstract class AbstractElement {

    protected final RenderHelper elementHelper;

    public AbstractElement(RenderHelper elementHelper) {
        this.elementHelper = elementHelper;
    }

    public abstract MacroDeclaration getMacro();

    public abstract String getFallback();

    @Override
    public String toString() {
        return elementHelper.display(this);
    }
}
