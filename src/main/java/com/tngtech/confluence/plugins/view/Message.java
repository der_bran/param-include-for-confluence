package com.tngtech.confluence.plugins.view;

import java.util.HashMap;
import java.util.Map;

import com.tngtech.confluence.plugins.process.MacroDeclaration;

/**
 * Uses confluence build-in macros to create a warning or info box and delivers its
 * HTML representation.
 */
public class Message extends AbstractElement {

	private static String spacerBetweenTitleAndText = ": ";
    private String spacerBetweenTextAndAppendix;
	private static String fallbackFormat = "<p>[%s]<p>%s</p>[/%s]</p>";

	private MessageType type = MessageType.INFO;

	private String title = "";
	private String body = "";
	private boolean showIcon = true;

	// derived classes can append content here via setAppendix(...)
	private String appendix = "";

	public enum MessageType {
		INFO("info"), NOTE("note"), WARNING("warning");

		private String macroName;

		private MessageType(String macroName) {
			this.macroName = macroName;
		}

		@Override
        public String toString() {
			return macroName;
		}
	}

	public Message(MessageType type, RenderHelper renderHelper) {
	    super(renderHelper);
		this.type = type;
		this.spacerBetweenTextAndAppendix = renderHelper.getLineBreak();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setBody(String body) {
		this.body = body;
	}

	protected void setAppendix(String appendix) {
		this.appendix = appendix;
	}

	public void showIcon() {
	    showIcon = true;
	}

	public void hideIcon() {
	    showIcon = false;
    }

	@Override
    public MacroDeclaration getMacro() {
	    Map<String, String> parameters = new HashMap<String, String>();
	    parameters.put("title", title);
	    parameters.put("icon", Boolean.toString(showIcon));
        return new MacroDeclaration(type.toString(), getTextWithAppendix(), "", parameters);
    }

	@Override
	public String getFallback() {
	    return String.format(fallbackFormat, type, getTextWithTitleAndAppendix(), type);
	}

	private String getTextWithAppendix() {
		StringBuilder builder = new StringBuilder();
		appendTextAndAppendixTo(builder);
		return builder.toString();
	}

	private String getTextWithTitleAndAppendix() {
		StringBuilder builder = new StringBuilder();
		if (!title.isEmpty()) {
			builder.append(title).append(spacerBetweenTitleAndText);
		}
		appendTextAndAppendixTo(builder);
		return builder.toString();
	}

	private StringBuilder appendTextAndAppendixTo(StringBuilder builder) {
		builder.append(body);
		if (!appendix.isEmpty()) {
			builder.append(spacerBetweenTextAndAppendix).append(appendix);
		}
		return builder;
	}
}
