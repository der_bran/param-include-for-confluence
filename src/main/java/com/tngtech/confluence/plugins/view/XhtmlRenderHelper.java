package com.tngtech.confluence.plugins.view;

import javax.xml.stream.XMLStreamException;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.definition.MacroBody;
import com.atlassian.confluence.content.render.xhtml.definition.RichTextMacroBody;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.tngtech.confluence.plugins.process.MacroDeclaration;
import com.tngtech.confluence.plugins.view.AbstractElement;
import com.tngtech.confluence.plugins.view.RenderHelper;

public class XhtmlRenderHelper implements RenderHelper {

    private final XhtmlContent xhtmlUtils;
    private ConversionContext context;

    public XhtmlRenderHelper(XhtmlContent xhtmlUtils, ConversionContext context) {
        this.xhtmlUtils = xhtmlUtils;
        this.context = context;
    }

    @Override
    public String display(AbstractElement element) {
        try {
            return xhtmlUtils.convertMacroDefinitionToView(transformMacro(element.getMacro()), context);
        } catch (XhtmlException e) {
            return element.getFallback();
        } catch (XMLStreamException e) {
            return element.getFallback();
        }
    }

    private MacroDefinition transformMacro(MacroDeclaration macro) throws XMLStreamException, XhtmlException {
        MacroBody body = new RichTextMacroBody(xhtmlUtils.convertStorageToView(macro.getBody(), context));
        return new MacroDefinition(macro.getName(), body, macro.getDefaultParameter(), macro.getParameters());
    }

    @Override
    public String render(String content) {
        try {
            return xhtmlUtils.convertStorageToView(content, context);
        } catch (XMLStreamException e) {
            return ""; // Don't output content unprocessed, cause of security reasons
        } catch (XhtmlException e) {
            return ""; // Don't output content unprocessed, cause of security reasons
        }
    }

    @Override
    public String getHighlightFormat() {
        return getHighlightFormatStatic();
    }

    public static String getHighlightFormatStatic() {
        return "<span style=\"color: rgb(255,0,0);\"><strong>%s</strong></span>";
    }

    @Override
    public String getBoldTextFormat() {
        return "<strong>%s</strong>";
    }

    @Override
    public String getLineBreak() {
        return "<br />";
    }

    @Override
    public String getPageLink(Page page, String url) {
        String format = "<strong><span class=\"icon icon-page\" title=\"Page\">Page:</span> <a href=\"%s\">%s</a> <span class=\"smalltext\">(%s)</span></strong>";
        return String.format(format, url, page.getTitle(), new SpaceDescription(page.getSpace()).getDisplayTitle());
    }
}
