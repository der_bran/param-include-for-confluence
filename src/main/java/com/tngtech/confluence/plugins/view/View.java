package com.tngtech.confluence.plugins.view;

import com.tngtech.confluence.plugins.view.Message.MessageType;


public class View {

    protected final RenderHelper renderHelper;

    private StringBuilder content = new StringBuilder();

    public View(RenderHelper renderHelper) {
        this.renderHelper = renderHelper;
    }

    public void appendMessage(MessageType type, String title, String text) {
        Message message = new Message(type, renderHelper);
        message.setTitle(title);
        message.setBody(text);
        append(message);
    }

    public void insertMessage(MessageType type, String title, String text) {
        Message message = new Message(type, renderHelper);
        message.setTitle(title);
        message.setBody(text);
        insert(message);
    }

    public void renderContent(String content) {
        append(renderHelper.render(content));
    }

    public void append(Object object) {
        content.append((object.toString()));
    }

    public void insert(Object object) {
        content.insert(0, (object.toString()));
    }

    @Override
    public String toString() {
        return content.toString();
    }
}
