package com.tngtech.confluence.plugins.view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.tngtech.confluence.plugins.process.MacroDeclaration;
import com.tngtech.confluence.plugins.process.StringHelper;

/**
 * Extends the MessageBlock by a list of parameter names.
 */
public class ParameterMessage extends Message {

    private String appendixFormat;
	private String delimiterBetweenParameter = ", ";

	private String label;
	private List<String> parameters = Collections.emptyList();

	public ParameterMessage(MessageType type, RenderHelper renderHelper) {
	    super(type, renderHelper);
        this.appendixFormat = renderHelper.getBoldTextFormat() + " %s";
	}

	public void setParameters(Collection<String> parameters, String label) {
		this.parameters = new ArrayList<String>(parameters);
        this.label = label;
	}

	@Override
	public MacroDeclaration getMacro() {
		setAppendix(String.format(appendixFormat, label, getSortedAndEscapedParametersAsString()));
		return super.getMacro();
	}

	@Override
    public String getFallback() {
	    setAppendix(String.format(appendixFormat, label, getSortedAndEscapedParametersAsString()));
        return super.getFallback();
    }

	private String getSortedAndEscapedParametersAsString() {
		Collections.sort(parameters);
		return StringHelper.escpapeSpecialChars(StringHelper.join(delimiterBetweenParameter, parameters));
	}
}
