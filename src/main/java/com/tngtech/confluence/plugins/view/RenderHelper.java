package com.tngtech.confluence.plugins.view;

import com.atlassian.confluence.pages.Page;

public interface RenderHelper {

    public String display(AbstractElement element);

    public String render(String content);

    public String getHighlightFormat();

    public String getBoldTextFormat();

    public String getLineBreak();

    public String getPageLink(Page page, String url);
}