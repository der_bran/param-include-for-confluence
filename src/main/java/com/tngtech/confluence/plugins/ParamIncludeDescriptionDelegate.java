package com.tngtech.confluence.plugins;

import com.tngtech.confluence.plugins.view.Message.MessageType;
import com.tngtech.confluence.plugins.view.RenderHelper;
import com.tngtech.confluence.plugins.view.View;

/**
 * Displays the macro body within an info macro.
 * This macro is intended to be filtered by Param-Include macro.
 */
public class ParamIncludeDescriptionDelegate {

	private final I18N i18n;

    protected ParamIncludeDescriptionDelegate(I18N i18n) {
        this.i18n = i18n;
    }

    public String execute(String body, RenderHelper renderHelper) {
        View view = new View(renderHelper);
        view.appendMessage(MessageType.INFO, i18n.getText(I18N.DESCRIPTION_TITLE), body);
        return view.toString();
    }
}