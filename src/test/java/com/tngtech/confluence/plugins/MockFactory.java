package com.tngtech.confluence.plugins;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.confluence.core.PartialList;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.user.User;
import com.tngtech.confluence.plugins.process.MacroDeclaration;
import com.tngtech.confluence.plugins.process.RegularExpression;
import com.tngtech.confluence.plugins.process.StringHelper;
import com.tngtech.confluence.plugins.view.AbstractElement;
import com.tngtech.confluence.plugins.view.RenderHelper;

public class MockFactory {

    public static RenderHelper mockRenderHelper() {
        return mockRenderHelper(false);
    }

    public static RenderHelper mockRenderHelperThrowingException() {
        return mockRenderHelper(true);
    }

    private static RenderHelper mockRenderHelper(boolean macroThrowsException) {
        RenderHelper mock = mock(RenderHelper.class);
        if (macroThrowsException) {
            when(mock.display(any(AbstractElement.class))).thenAnswer(createFallbackAnswer());
        } else {
            when(mock.display(any(AbstractElement.class))).thenAnswer(createMacroAnswer());
        }

        when(mock.getHighlightFormat()).thenReturn("[red][b]%s[/b][red]");
        when(mock.getBoldTextFormat()).thenReturn("[b]%s[/b]");
        when(mock.getPageLink(any(Page.class), anyString())).thenAnswer(createPageLinkAnswer());

        return mock;
    }

    private static Answer<String> createFallbackAnswer() {
        return new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) {
                return ((AbstractElement) invocation.getArguments()[0]).getFallback();
            }
        };
    }

    private static Answer<String> createMacroAnswer() {
        final String macroFormat = "[%s%s]body=%s[%s]";

        return new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) {
                MacroDeclaration macro = ((AbstractElement) invocation.getArguments()[0]).getMacro();
                String parameters = "";
                if (!macro.getParameters().isEmpty()) {
                    parameters = "," + StringHelper.join(",", "=", macro.getParameters());
                }
                return String.format(macroFormat, macro.getName(), parameters, macro.getBody(), macro.getName());
            }
        };
    }

    private static Answer<String> createPageLinkAnswer() {
        return new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) {
                Page page = ((Page) invocation.getArguments()[0]);
                String space = new SpaceDescription(((Page) invocation.getArguments()[0]).getSpace()).getDisplayTitle();
                String url = (String) invocation.getArguments()[1];
                String format = "[url:%s]%s[/url] (%s)";
                return String.format(format, url, page.getTitle(), space);
            }
        };
    }

    public static I18N mockI18N() {
        I18N mock = mock(I18N.class);
        when(mock.getMessageTitle(anyString())).thenAnswer(createRepresentiveSubstringAnswer());
        when(mock.getMessageBody(anyString())).thenAnswer(createRepresentiveSubstringAnswer());
        when(mock.getText(anyString())).thenAnswer(createRepresentiveSubstringAnswer());
        // when(mock.xxx(anyString(), any(Object[].class))).thenAnswer(createLastSubstringAnswer());
        return mock;
    }

    private static Answer<String> createRepresentiveSubstringAnswer() {
        return new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) {
                return getLastSubKey((String) invocation.getArguments()[0]);
            }
            private String getLastSubKey(String key) {
                int beginIndex = Math.min(key.lastIndexOf(".") + 1, key.length());
                return key.substring(beginIndex);
            }
        };
    }

    public enum PageMock {
        EXISTING, ONEPARAM, FORBIDDEN, MISSING, BLOG, RECURSION;

        @Override
        public String toString() {
            return this.name().toLowerCase();
        }
    }

    public static PageManager mockPageManager() {
        PageManager mock = mock(PageManager.class);
        when(mock.getPage(anyString(), eq(PageMock.EXISTING.toString())))
                .thenReturn(createPage(PageMock.EXISTING, "content"));
        when(mock.getPage(anyString(), eq(PageMock.ONEPARAM.toString())))
                .thenReturn(createPage(PageMock.ONEPARAM, "content" + encloseParameter("param") + "content"));
        when(mock.getPage(anyString(), eq(PageMock.FORBIDDEN.toString())))
                .thenReturn(createPage(PageMock.FORBIDDEN, "content"));
        when(mock.findPreviousBlogPost(anyString(), any(Date.class)))
                .thenAnswer(createNewBlogPostAnswer());
        when(mock.getPage(anyString(), eq(PageMock.RECURSION.toString())))
                .thenReturn(createPage(PageMock.RECURSION, "content"));
        return mock;
    }

    private static Page createPage(PageMock page, String body) {
        return createPage(page.toString(), body);
    }

    public static Page createPage(String title, String body) {
        Page page = new Page();
        Space space = new Space("SP");
        space.setName("space");
        page.setSpace(space);
        page.setTitle(title);
        page.setId(title.hashCode());
        setPageContent(page, body);
        return page;
    }

    // Necessary cause version specific PageHelper-Instance isn't available here
    private static void setPageContent(Page page, String body) {
        try {
            try {
                Page.class.getMethod("setBodyAsString", String.class).invoke(page, body);
            } catch (NoSuchMethodException e) {
                Page.class.getMethod("setContent", String.class).invoke(page, body);
            }
        } catch (NoSuchMethodException e) {
            // body not set, tests will fail
        } catch (IllegalAccessException e) {
            // body not set, tests will fail
        } catch (InvocationTargetException e) {
            // body not set, tests will fail
        }
    }

    private static String encloseParameter(String parameter) {
        return RegularExpression.PARAMETER_PLACEHOLDER_ENCLOSING + parameter
                + RegularExpression.PARAMETER_PLACEHOLDER_ENCLOSING;
    }

    private static Answer<BlogPost> createNewBlogPostAnswer() {
        return new Answer<BlogPost>() {
            @Override
            public BlogPost answer(InvocationOnMock invocation) {
                BlogPost mock = mock(BlogPost.class);
                when(mock.getTitle()).thenReturn("blog");
                return mock;
            }
        };
    }

    public static PermissionManager mockPermissionManager() {
        PermissionManager mock = mock(PermissionManager.class);
        when(mock.hasPermission(any(User.class), eq(Permission.VIEW), any(Page.class))).thenAnswer(
                createPermissionAnswer());
        return mock;
    }

    private static Answer<Boolean> createPermissionAnswer() {
        return new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocation) {
                if (((Page) invocation.getArguments()[2]).getTitle().contains("forbidden")) {
                    return false;
                } else {
                    return true;
                }
            }
        };
    }

    public static BootstrapManager mockBootstrapManager() {
        BootstrapManager mock = mock(BootstrapManager.class);
        when(mock.getWebAppContextPath()).thenReturn("base");
        return mock;
    }

    public static LabelManager mockLabelManager() {
        LabelManager mock = mock(LabelManager.class);
        when(mock.getContentForLabel(anyInt(), anyInt(), any(Label.class))).thenReturn(getLabeledPages());
        return mock;
    }

    public static LabelManager mockLabelManagerEmptyPageList() {
        LabelManager mock = mock(LabelManager.class);
        List<ContentEntityObject> pages = Collections.emptyList();
        PartialList<ContentEntityObject> partialPages = new PartialList<ContentEntityObject>(0,0, pages);
        when(mock.getContentForLabel(anyInt(), anyInt(), any(Label.class))).thenReturn(partialPages);
        return mock;
    }

    private static PartialList<ContentEntityObject> getLabeledPages() {
        List<ContentEntityObject> pages = new ArrayList<ContentEntityObject>();
        pages.add(createPage("page1", "content1"));
        pages.add(createPage("page3", "content3"));
        pages.add(createPage("page2", "content2"));
        PartialList<ContentEntityObject> partialPages = new PartialList<ContentEntityObject>(0, 0, pages);
        return partialPages;
    }

    public static AbstractElement mockAbstractElement() {
        return mockAbstractElement("macro");
    }

    public static AbstractElement mockAbstractElement(String name) {
        AbstractElement mock = mock(AbstractElement.class);
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("name1", "value1");
        parameters.put("name2", "value2");
        when(mock.getMacro()).thenReturn(createMacroDeclaration(name, "body", "default", parameters));
        when(mock.getFallback()).thenReturn("[macro]default:name1=value1:name2=value2:body[/macro]");
        return mock;
    }

    public static MacroDeclaration createMacroDeclaration(String name) {
        return createMacroDeclaration(name, "", "");
    }

    public static MacroDeclaration createMacroDeclaration(String name, String body, String defaultParameter) {
        Map<String, String> parameters = Collections.emptyMap();
        return createMacroDeclaration(name, body, defaultParameter, parameters);
    }

    public static MacroDeclaration createMacroDeclaration(String name, String body, String defaultParameter,
            Map<String, String> parameters) {
        return new MacroDeclaration(name, body, defaultParameter, parameters);
    }
}
