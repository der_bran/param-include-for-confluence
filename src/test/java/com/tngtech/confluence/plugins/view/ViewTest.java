package com.tngtech.confluence.plugins.view;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.tngtech.confluence.plugins.MockFactory;
import com.tngtech.confluence.plugins.view.Message.MessageType;

public class ViewTest {

    @Test
    public void testAppendMessage() {
        View view = createView();
        view.appendMessage(MessageType.INFO, "title", "text");
        assertContains("[" + MessageType.INFO + "]", view);
        assertContains("title=title", view);
        assertContains("body=text", view);
    }

    @Test
    public void testAppendMessageFallback() {
        View view = createFallbackView();
        view.appendMessage(MessageType.INFO, "title", "text");
        assertContains("[" + MessageType.INFO + "]", view);
        assertContains("title", view);
        assertContains("[/" + MessageType.INFO + "]", view);
    }

    @Test
    public void testAppendXhtml() {
        View view = createView();
        view.append("<p>text<strong>text</strong></p>");
        assertContains("<p>text<strong>text</strong></p>", view);
    }

    private View createView() {
        return new View(MockFactory.mockRenderHelper());
    }

    private View createFallbackView() {
        return new View(MockFactory.mockRenderHelperThrowingException());
    }

    private static void assertContains(String subsequence, View view) {
        assertTrue("Expected: [" + subsequence + "] in: [" + view + ']', view.toString().contains(subsequence));
    }
}
