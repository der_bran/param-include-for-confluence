package com.tngtech.confluence.plugins.view;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.tngtech.confluence.plugins.MockFactory;
import com.tngtech.confluence.plugins.view.Message.MessageType;

public class MessageTest {

    private static final String spacerBetweenTitleAndText = ": ";

    @Test
    public void testWarningMessage() {
        Message message = createMessage(MessageType.WARNING);
        assertMessageContains("[" + MessageType.WARNING + "]", message);
    }

    @Test
    public void testInfoMessage() {
        Message message = createMessage(MessageType.INFO);
        assertMessageContains("[" + MessageType.INFO + "]", message);
    }

    @Test
    public void testNoteMessage() {
        Message message = createMessage(MessageType.NOTE);
        assertMessageContains("[" + MessageType.NOTE + "]", message);
    }

    @Test
    public void testMessageWithText() {
        Message message = createMessage(MessageType.INFO);
        message.setBody("text");
        assertMessageContains("[" + MessageType.INFO + "]", message);
        assertMessageContains("text", message);
    }

    @Test
    public void testMessageWithRichText() {
        Message message = createMessage(MessageType.INFO);
        message.setBody("<p>text<strong>text</strong></p>");
        assertMessageContains("[" + MessageType.INFO + "]", message);
        assertMessageContains("body=<p>text<strong>text</strong></p>", message);
    }

    @Test
    public void testMessageWithTitle() {
        Message message = createMessage(MessageType.INFO);
        message.setTitle("title");
        assertMessageContains("[" + MessageType.INFO + "]", message);
        assertMessageContains("title=title", message);
    }

    @Test
    public void testMessageWithTitleAndText() {
        Message message = createMessage(MessageType.INFO);
        message.setTitle("title");
        message.setBody("text");
        assertMessageContains("[" + MessageType.INFO + "]", message);
        assertMessageContains("body=text", message);
        assertMessageContains("title=title", message);
    }

    @Test
    public void testFallBack() {
        Message message = createFallbackMessage(MessageType.INFO);
        message.setTitle("title");
        message.setBody("text");
        assertMessageContains("[" + MessageType.INFO + "]<p>title" + spacerBetweenTitleAndText
                + "text</p>[/" + MessageType.INFO + ']', message);
    }

    private Message createMessage(MessageType type) {
        return new Message(type, MockFactory.mockRenderHelper());
    }

    private Message createFallbackMessage(MessageType type) {
        return new Message(type, MockFactory.mockRenderHelperThrowingException());
    }

    private static void assertMessageContains(String subsequence, Message message) {
        assertTrue("Expected: [" + subsequence + "] in: [" + message + ']', message.toString()
                .contains(subsequence));
    }
}
