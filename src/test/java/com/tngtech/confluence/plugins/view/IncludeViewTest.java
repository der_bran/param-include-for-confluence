package com.tngtech.confluence.plugins.view;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import com.tngtech.confluence.plugins.MockFactory;
import com.tngtech.confluence.plugins.view.Message.MessageType;

public class IncludeViewTest {

    @Test
    public void testAppendMessage() {
        IncludeView view = createIncludeView();
        view.insertMessage(MessageType.INFO, "title", "content", "label", getTestParameters());
        assertContains("[" + MessageType.INFO + "]", view);
        assertContains("title=title", view);
        assertContains("body=content", view);
        assertContains("label", view);
        assertContainsParameters(getTestParameters(), view);
    }

    @Test
    public void testAppendMessageFallback() {
        IncludeView view = createFallbackIncludeView();
        view.insertMessage(MessageType.INFO, "title", "content", "label", getTestParameters());
        assertContains("[" + MessageType.INFO + "]", view);
        assertContains("title: content", view);
        assertContains("label", view);
        assertContainsParameters(getTestParameters(), view);
        assertContains("[/" + MessageType.INFO + "]", view);
    }

    private IncludeView createIncludeView() {
        return new IncludeView(MockFactory.mockRenderHelper());
    }

    private IncludeView createFallbackIncludeView() {
        return new IncludeView(MockFactory.mockRenderHelperThrowingException());
    }

    private Collection<String> getTestParameters() {
        Collection<String> parameters = new ArrayList<String>();
        parameters.add("param1");
        parameters.add("param2");
        return parameters;
    }

    private static void assertContainsParameters(Collection<String> referenceParameters, IncludeView view) {
        for (String parameter : referenceParameters) {
            assertContains(parameter, view);
        }
    }

    private static void assertContains(String subsequence, IncludeView view) {
        assertTrue("Expected: [" + subsequence + "] in: [" + view + ']', view.toString().contains(subsequence));
    }
}
