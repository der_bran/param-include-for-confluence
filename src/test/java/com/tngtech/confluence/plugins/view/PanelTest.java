package com.tngtech.confluence.plugins.view;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.tngtech.confluence.plugins.MockFactory;

public class PanelTest {

    @Test
    public void testLabelWithContent() {
        Panel panel = createPanel();
        panel.setContent("text");
        assertPanelContains("[panel]", panel);
        assertPanelContains("body=text", panel);
    }

    @Test
    public void testFallBack() {
        Panel panel = createFallbackPanel();
        panel.setContent("text");
        assertPanelContains("text", panel);
    }

    private Panel createPanel() {
        return new Panel(MockFactory.mockRenderHelper());
    }

    private Panel createFallbackPanel() {
        return new Panel(MockFactory.mockRenderHelperThrowingException());
    }

    private static void assertPanelContains(String subsequence, Panel panel) {
        assertTrue("Expected: [" + subsequence + "] in: [" + panel + ']', panel.toString().contains(subsequence));
    }
}
