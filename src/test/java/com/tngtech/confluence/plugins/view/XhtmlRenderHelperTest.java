package com.tngtech.confluence.plugins.view;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.tngtech.confluence.plugins.XhtmlMockFactory;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.tngtech.confluence.plugins.MockFactory;

public class XhtmlRenderHelperTest {

    XhtmlRenderHelper renderHelperUnderTest;

    @Before
    public void setUp() {
        XhtmlContent xhtmlUtils = XhtmlMockFactory.mockXhtmlContent();
        ConversionContext conversionContext = XhtmlMockFactory.mockConversionContext();
        renderHelperUnderTest = new XhtmlRenderHelper(xhtmlUtils, conversionContext);
    }

    @Test
    public void testDisplayElement() {
        AbstractElement element = MockFactory.mockAbstractElement();
        String result = renderHelperUnderTest.display(element);
        assertContains("MacroDefinition", result);
        assertContains("name=macro", result);
        assertContains("body=RichTextMacroBody[storageBody=<null>,transformedBody=StringStreamable{body}]", result);
        assertContains("name1=value1", result);
        assertContains("name2=value2", result);
    }

    @Test
    public void testPageLink() {
        Page page = MockFactory.createPage("title", "content");
        String expected = "<strong><span class=\"icon icon-page\" title=\"Page\">Page:</span> <a href=\"base/\">title</a> <span class=\"smalltext\">(space)</span></strong>";
        assertEquals(expected, renderHelperUnderTest.getPageLink(page, "base/"));
    }

    private static void assertContains(String subsequence, String sequence) {
        assertTrue("Expected: [" + subsequence + "] in: [" + sequence + ']', sequence.contains(subsequence));
    }
}
