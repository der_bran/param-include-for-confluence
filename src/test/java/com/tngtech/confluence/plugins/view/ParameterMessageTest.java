package com.tngtech.confluence.plugins.view;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import com.tngtech.confluence.plugins.MockFactory;
import com.tngtech.confluence.plugins.view.Message.MessageType;

public class ParameterMessageTest {

    private static String delimiterBetweenParameter = ", ";

    @Test
    public void testWithOneParameter() {
        List<String> parameters = getTestParameters(1);
        ParameterMessage message = createParameterMessage(MessageType.INFO);
        message.setParameters(parameters, "label");
        assertMessageContains("label", message);
        assertMessageContains(parameters.get(0), message);
    }

    @Test
    public void testWithTwoParameter() {
        List<String> parameters = getTestParameters(2);
        ParameterMessage message = createParameterMessage(MessageType.INFO);
        message.setParameters(parameters, "label");
        assertMessageContains("label", message);
        assertMessageContains(parameters.get(0) + delimiterBetweenParameter + parameters.get(1), message);
    }

    @Test
    public void testParameterSorting() {
        List<String> parameters = getTestParameters(2);
        List<String> reversed = getTestParameters(2);
        Collections.reverse(reversed);
        ParameterMessage message = createParameterMessage(MessageType.INFO);
        message.setParameters(reversed, "label");
        assertMessageContains("label", message);
        assertMessageContains(parameters.get(0) + delimiterBetweenParameter + parameters.get(1), message);
    }

    @Test
    public void testWithTextAndParameter() {
        List<String> parameters = getTestParameters(1);
        ParameterMessage message = createParameterMessage(MessageType.INFO);
        message.setBody("text");
        message.setParameters(parameters, "label");
        assertMessageContains("text", message);
        assertMessageContains("label", message);
        assertMessageContains(parameters.get(0), message);
    }

    @Test
    public void testHtmlEscapeParameter() {
        List<String> parameters = getTestParameters(0);
        parameters.add("</p></ac:rich-text-body></ac:macro>injection");
        ParameterMessage message = createParameterMessage(MessageType.INFO);
        message.setParameters(parameters, "label");
        assertMessageContains("label", message);
        assertMessageContains("&lt;/p&gt;&lt;/ac:rich-text-body&gt;&lt;/ac:macro&gt;injection", message);
    }

    private List<String> getTestParameters(int count) {
        List<String> parameters = new ArrayList<String>();
        while (parameters.size() < count) {
            parameters.add("param" + (parameters.size() + 1));
        }
        return parameters;
    }

    private ParameterMessage createParameterMessage(MessageType type) {
        return new ParameterMessage(type, MockFactory.mockRenderHelper());
    }

    private static void assertMessageContains(String subsequence, ParameterMessage message) {
        assertTrue("Expected: [" + subsequence + "] in: [" + message + ']', message.toString()
                .contains(subsequence));
    }
}
