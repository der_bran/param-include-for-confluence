package com.tngtech.confluence.plugins.view;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.tngtech.confluence.plugins.MockFactory;
import com.tngtech.confluence.plugins.view.Message.MessageType;

public class CatalogViewTest {

    @Test
    public void testAppendPageUrlBox() {
        CatalogView view = createCatalogView();
        view.appendPageUrlBox(MockFactory.createPage("title", "content"), "base");
        assertContains("[" + MessageType.INFO + "]", view);
        assertContains("icon=false", view);
        assertContains("[url:base/display/SP/title]", view);
        assertContains("title", view);
        assertContains("(space)", view);
    }

    @Test
    public void testAppendPageUrlBoxFallback() {
        CatalogView view = createFallbackCatalogView();
        view.appendPageUrlBox(MockFactory.createPage("title", "content"), "base");
        assertContains("[" + MessageType.INFO + "]", view);
        assertContains("[url:base/display/SP/title]", view);
        assertContains("title", view);
        assertContains("(space)", view);
        assertContains("[/" + MessageType.INFO + "]", view);
    }

    @Test
    public void testAppendPanel() {
        CatalogView view = createCatalogView();
        view.appendPanel("text");
        assertContains("[panel]", view);
        assertContains("body=text", view);
    }

    @Test
    public void testAppendPanelWithRichText() {
        CatalogView view = createCatalogView();
        view.appendPanel("<p>text<strong>text</strong></p>");
        assertContains("[panel]", view);
        assertContains("body=<p>text<strong>text</strong></p>", view);
    }

    @Test
    public void testAppendPanelFallback() {
        CatalogView view = createFallbackCatalogView();
        view.appendPanel("text");
        assertContains("text", view);
    }

    @Test
    public void testVerticalSpace() {
        CatalogView view = createCatalogView();
        view.appendVerticalSpace();
        assertContains("<p>&nbsp;</p>", view);
    }

    private CatalogView createCatalogView() {
        return new CatalogView(MockFactory.mockRenderHelper());
    }

    private CatalogView createFallbackCatalogView() {
        return new CatalogView(MockFactory.mockRenderHelperThrowingException());
    }

    private static void assertContains(String subsequence, CatalogView view) {
        assertTrue("Expected: [" + subsequence + "] in: [" + view + ']', view.toString().contains(subsequence));
    }
}
