package com.tngtech.confluence.plugins;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;

import com.tngtech.confluence.plugins.MockFactory;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.definition.RichTextMacroBody;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionReplacer;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.tngtech.confluence.plugins.process.ParameterDeclaration;
import com.tngtech.confluence.plugins.XhtmlParamIncludeMacro;

public class XhtmlMockFactory {


    public static XhtmlContent mockXhtmlContent() {
        XhtmlContent mock = mock(XhtmlContent.class);
        try {
            when(mock.convertMacroDefinitionToView(any(MacroDefinition.class),
                    any(ConversionContext.class)))
                    .thenAnswer(createFirstParameterToStringAnswer());
            when(mock.convertMacroDefinitionToStorage(any(MacroDefinition.class),
                    any(ConversionContext.class)))
                    .thenAnswer(createFirstParameterToStringAnswer());
            when(mock.convertStorageToView(anyString(), any(ConversionContext.class)))
                    .thenAnswer(createFirstParameterToStringAnswer());
            when(mock.replaceMacroDefinitionsWithString(anyString(), any(ConversionContext.class),
                    any(MacroDefinitionReplacer.class))).thenAnswer(createInvokeReplacerAnswer());
        } catch (XMLStreamException e) {
            fail("Mock could not be created.");
        } catch (XhtmlException e) {
            fail("Mock could not be created.");
        }
        return mock;
    }

    private static Answer<String> createFirstParameterToStringAnswer() {
        return new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) {
                return invocation.getArguments()[0].toString();
            }
        };
    }

    private static Answer<String> createInvokeReplacerAnswer() {
        return new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) {
                try {
                    String body = (String) invocation.getArguments()[0];
                    MacroDefinition macro = extractMacroDefinitionFromString(body);
                    if (macro != null) {
                        return ((MacroDefinitionReplacer) invocation.getArguments()[2]).replace(macro);
                    } else {
                        return body;
                    }
                } catch (XhtmlException e) {
                    return "";
                }
            }
        };
    }

    private static MacroDefinition extractMacroDefinitionFromString(String body) {
        Pattern pattern = Pattern.compile("MacroDefinition\\[.*name=([^,]*)");
        Matcher matcher = pattern.matcher(body);
        if (matcher.find()) {
            return createMacroDefinition(matcher.group(1));
        } else {
            return null;
        }
    }

    public static MacroDefinition createMacroDefinition(String name) {
        Map<String, String> parameters = Collections.emptyMap();
        return new MacroDefinition(name, new RichTextMacroBody(""), "", parameters);
    }

    public static ConversionContext mockConversionContext() {
        ConversionContext mock = mock(ConversionContext.class);
        when(mock.hasProperty(XhtmlParamIncludeMacro.RECURSION_STACK)).thenReturn(true);
        when(mock.getProperty(XhtmlParamIncludeMacro.RECURSION_STACK)).thenReturn(createRecursionStack());
        when(mock.hasProperty(XhtmlParamIncludeMacro.PARAMETER_INHERITANCE)).thenReturn(true);
        Stack<Map<String, ParameterDeclaration>> parameters = new Stack<Map<String, ParameterDeclaration>>();
        when(mock.getProperty(XhtmlParamIncludeMacro.PARAMETER_INHERITANCE)).thenReturn(parameters);
        return mock;
    }

    private static Stack<String> createRecursionStack() {
        Stack<String> stack = new Stack<String>();
        long pageId = MockFactory.PageMock.RECURSION.toString().hashCode();
        stack.push(Long.toString(pageId));
        return stack;
    }
}
