package com.tngtech.confluence.plugins;

import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.tngtech.confluence.plugins.view.Message.MessageType;

public class XhtmlParamIncludeCatalogMacroTest {

    private XhtmlParamIncludeCatalogMacro xhtmlMacroUnderTest = null;

    @Before
    public void setUp() {
        setUp(MockFactory.mockLabelManager());
    }

    public void setUp(LabelManager labelManager) {
        PermissionManager permissionManager = MockFactory.mockPermissionManager();
        BootstrapManager bootstrapManager = MockFactory.mockBootstrapManager();
        XhtmlContent xhtmlUtils = XhtmlMockFactory.mockXhtmlContent();
        I18N i18n = MockFactory.mockI18N();
        xhtmlMacroUnderTest = new XhtmlParamIncludeCatalogMacro(labelManager, permissionManager, bootstrapManager, xhtmlUtils, i18n);
    }

    @Test
    public void testPageElementsExist() {
        String result = executeMacro();
        assertContains("MacroDefinition", result);
        assertContains("name=info", result);
        assertContains("icon=false", result);
        assertContains("name=panel", result);
        assertContains("page1", result);
        assertContains("(space)", result);
        assertContains("base/display/SP/page1", result);
        assertContains("content1", result);
        assertContains("page2", result);
        assertContains("(space)", result);
        assertContains("base/display/SP/page2", result);
        assertContains("content2", result);
    }

    @Test
    public void testCatalogSorting() {
        String result = executeMacro();
        assertContainsInOrder("page1", "page2", result);
        assertContainsInOrder("page1", "page3", result);
        assertContainsInOrder("page2", "page3", result);
        assertContainsInOrder("content1", "content2", result);
        assertContainsInOrder("content1", "content3", result);
        assertContainsInOrder("content2", "content3", result);
        assertContainsInOrder("base/display/SP/page1", "base/display/SP/page2", result);
        assertContainsInOrder("base/display/SP/page1", "base/display/SP/page3", result);
        assertContainsInOrder("base/display/SP/page2", "base/display/SP/page3", result);
    }

    @Test
    public void testEmptyCatalog() {
        setUp(MockFactory.mockLabelManagerEmptyPageList());
        String result = executeMacro();
        assertMessage(MessageType.NOTE, result);
        assertContains(getI18NMessageTitle(I18N.EMPTY_CATALOG), result);
    }

    private String executeMacro() {
        ConversionContext context = XhtmlMockFactory.mockConversionContext();
        Map<String, String> parameters = Collections.emptyMap();
        return xhtmlMacroUnderTest.execute(parameters, "", context);
    }

    private static void assertMessage(MessageType type, String result) {
        assertContains(type.toString(), result);
    }

    private static void assertContains(String subsequence, String result) {
        assertTrue("Expected: [" + subsequence + "] in: [" + result + ']', result.contains(subsequence));
    }

    private static void assertContainsInOrder(String first, String second, String result) {
        assertTrue("Expected: [" + first + "] in: [" + result + ']', result.contains(first));
        assertTrue("Expected: [" + second + "] in: [" + result + ']', result.contains(second));
        assertTrue("Expected: [" + first + "] before: [" + second + "]", result.indexOf(first) < result.indexOf(second));
    }

    private static String getI18NMessageTitle(String key) {
        return MockFactory.mockI18N().getMessageTitle(key);
    }
}
