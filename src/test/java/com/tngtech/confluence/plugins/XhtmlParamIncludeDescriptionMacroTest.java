package com.tngtech.confluence.plugins;

import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.tngtech.confluence.plugins.view.Message.MessageType;

public class XhtmlParamIncludeDescriptionMacroTest {

    private XhtmlParamIncludeDescriptionMacro xhtmlMacroUnderTest;

    @Before
    public void init() {
        XhtmlContent xhtmlUtils = XhtmlMockFactory.mockXhtmlContent();
        I18N i18n = MockFactory.mockI18N();
        xhtmlMacroUnderTest = new XhtmlParamIncludeDescriptionMacro(xhtmlUtils, i18n);
    }

	@Test
	public void testValidDescription() {
		Map<String, String> parameters = Collections.emptyMap();
		String body = "<p>This is my <strong>Description</strong>.</p>";
		ConversionContext context = XhtmlMockFactory.mockConversionContext();
        String result = xhtmlMacroUnderTest.execute(parameters, body, context);
        assertResultContains("MacroDefinition", result);
        assertResultContains("body=RichTextMacroBody[storageBody=<null>,transformedBody=StringStreamable{" + body + "}]", result);
        assertResultContains("name=" + MessageType.INFO, result);
        assertResultContains("title=" + MockFactory.mockI18N().getText((I18N.DESCRIPTION_TITLE)), result);
	}

	private static void assertResultContains(String subsequence, String result) {
	    assertTrue("Expected: [" + subsequence + "] in: [" + result + ']', result.contains(subsequence));
	}
}
