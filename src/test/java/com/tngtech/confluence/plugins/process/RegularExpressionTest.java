package com.tngtech.confluence.plugins.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

public class RegularExpressionTest {

    @Test
    public void testDeclarationDelimiter() {
        Pattern pattern = RegularExpression.DECLARATION_DELIMITER;
        for (SplitTest test : getDeclarationDelimiterList()) {
            String[] parameters = pattern.split(test.declaration);
            assertEquals("Not splitable: [" + test.declaration + "]", test.count, parameters.length);
        }
    }

    private static class SplitTest {
        private String declaration;
        private int count;

        public SplitTest(String x, int y) {
            this.declaration = x;
            this.count = y;
        }
    }

    private List<SplitTest> getDeclarationDelimiterList() {
        List<SplitTest> sequences = new ArrayList<SplitTest>();
        sequences.add(new SplitTest("test|test", 2));
        sequences.add(new SplitTest("|test", 2));
        sequences.add(new SplitTest("test| ", 2));
        sequences.add(new SplitTest("test\\|test", 1));
        sequences.add(new SplitTest("test\\\\|test", 2));
        sequences.add(new SplitTest("test\\\\\\|test", 1));
        return sequences;
    }

    @Test
    public void testUnescapeDelimiter() {
        Pattern pattern = RegularExpression.UNESCAPE_DELIMITER;
        for (ReplaceTest test : getUnescapeDelimiterList()) {
            Matcher matcher = pattern.matcher(test.escaped);
            String unescaped = matcher.replaceAll(RegularExpression.DELIMITER_SEQUENCE);
            assertEquals("Not escapable: ", test.unescaped, unescaped);
       }
    }

    private static class ReplaceTest {
        private String escaped;
        private String unescaped;

        public ReplaceTest(String escaped, String unescaped) {
            this.escaped = escaped;
            this.unescaped = unescaped;
        }
    }

    private List<ReplaceTest> getUnescapeDelimiterList() {
        List<ReplaceTest> sequences = new ArrayList<ReplaceTest>();
        sequences.add(new ReplaceTest("test\\|test", "test|test"));
        sequences.add(new ReplaceTest("test\\\\|test", "test\\\\|test"));
        sequences.add(new ReplaceTest("test\\\\\\|test", "test\\\\|test"));
        return sequences;
    }

    @Test
    public void testParameterPlaceholder() {
        Pattern pattern = RegularExpression.PARAMETER_PLACEHOLDER;
        for (String sequence : getParameterPlaceholderList()) {
            Matcher matcher = pattern.matcher(sequence);
            assertTrue("Not found: [" + sequence + "]", matcher.find());
       }
    }

    private List<String> getParameterPlaceholderList() {
        List<String> sequences = new ArrayList<String>();
        sequences.add(" $param$ ");
        sequences.add("$param$");
        sequences.add("test$param$test");
        sequences.add("test $param$ test");
        sequences.add("test,$param$,test");
        sequences.add("$_$");
        sequences.add("$_param_$");
        sequences.add("$pa_ram$");
        sequences.add("test,$5345$,test");
        sequences.add("test,$ÖÄÜß$,test");
        sequences.add("test,$夫艅韓$,test");
        return sequences;
    }

    @Test
    public void testInvalidParameterPlaceholder() {
        Pattern pattern = RegularExpression.PARAMETER_PLACEHOLDER;
        for (String sequence : getInvalidParameterPlaceholderList()) {
            Matcher matcher = pattern.matcher(sequence);
            assertFalse("Not found: [" + sequence + "]", matcher.find());
       }
    }

    private List<String> getInvalidParameterPlaceholderList() {
        List<String> sequences = new ArrayList<String>();
        sequences.add("@ param@");
        sequences.add("@param @");
        sequences.add(" @ param @ ");
        sequences.add(" @\nparam@ ");
        sequences.add("test,@$param@,test");
        return sequences;
    }

    @Test
    public void testParameterDeclaration() {
        Pattern pattern = RegularExpression.PARAMETER_DECLARATION;
        for (String sequence : getParameterDeclarationList()) {
            Matcher matcher = pattern.matcher(sequence);
            assertTrue("Unmatched: [" + sequence + "]", matcher.matches());
       }
    }

    private List<String> getParameterDeclarationList() {
        List<String> sequences = new ArrayList<String>();
        sequences.add("param=value");
        sequences.add("param = value");
        sequences.add(" param    \n\n\t=\n\r value\n \r");
        sequences.add("_=value");
        sequences.add("1=value");
        sequences.add("夫=@ÖÄÜß@,");
        sequences.add("夫=艅韓");
        sequences.add("夫 = 艅€韓");
        return sequences;
    }
}
