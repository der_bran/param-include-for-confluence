package com.tngtech.confluence.plugins.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.tngtech.confluence.plugins.XhtmlMockFactory;
import org.junit.Test;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

public class XhtmlMacroFilterTest {

    private final static String presentMacro = "test";
    private final static String nonpresentMacro = "other";

    @Test
    public void testFilterTargetMacro() {
        MacroFilter filter = createMacroFilter(presentMacro);
        MacroDefinition macro = XhtmlMockFactory.createMacroDefinition(presentMacro);
        String result = filter.filter(macro.toString());
        assertEquals("", result);
        assertCapture(XhtmlMockFactory.createMacroDefinition(presentMacro), filter);
    }

    @Test
    public void testPreserveOtherMacro() {
        MacroFilter filter = createMacroFilter(nonpresentMacro);
        MacroDefinition macro = XhtmlMockFactory.createMacroDefinition(presentMacro);
        String result = filter.filter(macro.toString());
        assertEquals(macro.toString(), result);
        assertCapture(null, filter);
    }

    private XhtmlMacroFilter createMacroFilter(String macro) {
        XhtmlContent xhtmlUtils = XhtmlMockFactory.mockXhtmlContent();
        ConversionContext context = XhtmlMockFactory.mockConversionContext();
        return new XhtmlMacroFilter(macro, xhtmlUtils, context);
    }

    private static void assertCapture(MacroDefinition macro, MacroFilter filter) {
        MacroDeclaration capture = filter.getCapture();
        if (macro == null && capture == null) {
            return;
        }
        assertNotNull(macro);
        assertNotNull(capture);
        assertEquals(macro.getName(), capture.getName());
        assertEquals(macro.getBodyText(), capture.getBody());
        assertEquals(macro.getDefaultParameterValue(), capture.getDefaultParameter());
        assertEquals(macro.getParameters(), capture.getParameters());
    }
}
