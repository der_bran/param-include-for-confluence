package com.tngtech.confluence.plugins.process;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class PageIdentTest {

    private static final String defaultSpaceKey = "default";

	@Test
	public void testPageWithNoSpaceKey() throws NoPageParameterException {
	    PageIdent ident = createPageIdent("page title");
	    assertPageName("page title", ident);
	    assertSpaceKey(defaultSpaceKey, ident);
	}

	@Test
	public void testPageWithEmptySpaceKey() throws NoPageParameterException {
	    PageIdent ident = createPageIdent(":page title");
		assertPageName("page title", ident);
		assertSpaceKey(defaultSpaceKey, ident);
	}

	@Test
	public void testPageWithSpaceKey() throws NoPageParameterException {
	    PageIdent ident = createPageIdent("space key:page title");
		assertPageName("page title", ident);
		assertSpaceKey("space key", ident);
	}

	@Test(expected = NoPageParameterException.class)
	public void testEmptyPageIdent() throws NoPageParameterException {
	    createPageIdent("");
	}

	private PageIdent createPageIdent(String page) throws NoPageParameterException {
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("page", page);
		return new PageIdent(parameters, defaultSpaceKey);
	}

	private static void assertPageName(String name, PageIdent ident) {
		assertEquals(name, ident.getTitle());
	}

	private static void assertSpaceKey(String key, PageIdent ident) {
		assertEquals(key, ident.getSpaceKey());
	}
}
