package com.tngtech.confluence.plugins.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;

import com.tngtech.confluence.plugins.process.Notification.NotificationType;

public class ParameterListingTest {

	@Test
	public void testEmptyString() {
        ParameterListing listing = ParameterListing.parse("");
		assertListingEquals(Collections.<String, String>emptyMap(), listing);
		assertZeroError(listing);
	}

	@Test
	public void testwhiteSpace() {
        ParameterListing listing = ParameterListing.parse(" ");
		assertListingEquals(Collections.<String, String>emptyMap(), listing);
        assertZeroError(listing);
	}

	@Test
	public void testNonBreakingSpaceIsEmpty() {
        // Confluence uses e.g. \u00a0-Characters which are non-whitespace in Java
        ParameterListing listing = ParameterListing.parse("\u00A0\u2007\u202F");
		assertListingEquals(Collections.<String, String>emptyMap(), listing);
        assertZeroError(listing);
	}

    @Test
    public void testEmptyValue() {
        ParameterListing listing = ParameterListing.parse("param1=");
        assertZeroError(listing);
    }

    @Test
    public void testEmptyName() {
        ParameterListing listing = ParameterListing.parse("=value");
        assertError(NotificationType.INVALID_DECLARATION, listing);
    }

    @Test
    public void testParseError() {
        ParameterListing listing = ParameterListing.parse("param1");
        assertError(NotificationType.INVALID_DECLARATION, listing);
    }

	@Test
	public void testParametersAgainstReference() {
        ParameterListing listing = ParameterListing.parse("param1=value1|param2=value2 value2 | param3 = value3 value3 ");
		assertListingEquals(getReferenceParameterMap(), listing);
        assertZeroError(listing);
	}

	private Map<String, String> getReferenceParameterMap() {
		Map<String, String> references = new HashMap<String, String>();
        references.put("param1", "value1");
		references.put("param2", "value2 value2");
		references.put("param3", "value3 value3");
        return references;
	}

	@Test
	public void testParametersWithEscapedDelimiters() {
        ParameterListing listing = ParameterListing.parse("param1=1\\|param1=x|param2=2\\\\|param2=x|param3=3\\\\\\|param3=x");
		assertListingEquals(getReferenceParameterMapForEscapedDelimiters(), listing);
        assertZeroError(listing);
	}

	private Map<String, String> getReferenceParameterMapForEscapedDelimiters() {
		Map<String, String> references = new HashMap<String, String>();
        references.put("param1", "1|param1=x");
		references.put("param2", "x");
		references.put("param3", "3\\\\|param3=x");
        return references;
	}

   @Test
    public void testParametersWithMultilineValues() {
        ParameterListing listing = ParameterListing.parse("param1=test1|param2=zeile1 \nzeile2|param3=test3");
        assertListingEquals(getReferenceParameterMapForMultilineValues(), listing);
        assertZeroError(listing);
    }

    private Map<String, String> getReferenceParameterMapForMultilineValues() {
        Map<String, String> references = new HashMap<String, String>();
        references .put("param1", "test1");
        references.put("param2", "zeile1 \nzeile2");
        references.put("param3", "test3");
        return references;
    }

	private static void assertListingEquals(Map<String, String> references, ParameterListing listing) {
	    Map<String, ParameterDeclaration> parameters = listing.getIncludeParameters();
		assertEquals(references.size(), parameters.size());
		int usedParameterEntryCount = 0;
		for (Entry<String, String> referenceParameter : references.entrySet()) {
		    assertParameterPresent(referenceParameter, parameters);
			usedParameterEntryCount++;
		}
		assertEquals(parameters.size(), usedParameterEntryCount);
	}

	private static void assertParameterPresent(Entry<String, String> referenceEntry, Map<String, ParameterDeclaration> parameters) {
		String referenceKey = referenceEntry.getKey();
		String referenceValue = referenceEntry.getValue();
		String value = parameters.get(referenceKey).getValue();
		assertEquals(referenceValue, value);
	}

	private static void assertZeroError(ParameterListing listing) {
	    assertTrue("Unexpected errors occured", listing.getNotifications().isEmpty());
	}

    private static void assertError(NotificationType type, ParameterListing listing) {
        List<Notification> errors = listing.getNotifications();
        for (Notification error : errors) {
            if (error.getType().equals(type)) {
                return;
            }
        }
        fail("Expected Error with type " + type);
    }
}
