package com.tngtech.confluence.plugins.process;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ParameterDeclarationTest {

	@Test
	public void testDeclaration() throws InvalidDeclarationException {
	    ParameterDeclaration declaration = ParameterDeclaration.parse("name=value");
		assertName("name", declaration);
		assertValue("value", declaration);
	}

	@Test
	public void testDeclarationWithWhiteSpaces() throws InvalidDeclarationException {
        ParameterDeclaration declaration = ParameterDeclaration.parse("name = value");
		assertName("name", declaration);
		assertValue("value", declaration);
	}

	@Test
	public void testDeclarationWithEmptyValue() throws InvalidDeclarationException {
        ParameterDeclaration declaration = ParameterDeclaration.parse(" name =");
		assertName("name", declaration);
		assertValue("", declaration);
	}

    @Test
	public void testValueWithEndingDot() throws InvalidDeclarationException {
        ParameterDeclaration declaration = ParameterDeclaration.parse(" name = value. ");
		assertName("name", declaration);
		assertValue("value.", declaration);
	}

	@Test
	public void testValueWithMultipleEndingDots() throws InvalidDeclarationException {
        ParameterDeclaration declaration = ParameterDeclaration.parse(" name = value ... ");
		assertName("name", declaration);
		assertValue("value ...", declaration);
	}

    @Test
    public void testValueWithTrailingNonBreakingSpace() throws InvalidDeclarationException {
        ParameterDeclaration declaration = ParameterDeclaration.parse("\u00A0name=value");
        assertName("name", declaration);
        assertValue("value", declaration);
    }

    @Test
    public void testValueWithNonBreakingSpaces() throws InvalidDeclarationException {
        ParameterDeclaration declaration = ParameterDeclaration.parse("\u00A0name\u00A0=\u00A0value\u00A0.\u00A0");
        assertName("name", declaration);
        assertValue("value\u00A0.", declaration);
    }

    @Test(expected=InvalidDeclarationException.class)
    public void testInvalidDeclarations() throws InvalidDeclarationException {
        ParameterDeclaration.parse("name:value");
    }

    @Test(expected=InvalidDeclarationException.class)
    public void testMissingParameterName() throws InvalidDeclarationException {
        ParameterDeclaration.parse("value");
    }

	private static void assertName(String name, ParameterDeclaration declaration) {
		assertEquals(name, declaration.getName());
	}

	private static void assertValue(String value, ParameterDeclaration declaration) {
		assertEquals(value,declaration.getValue());
	}
}
