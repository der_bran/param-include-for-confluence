package com.tngtech.confluence.plugins.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.junit.Test;

import com.tngtech.confluence.plugins.process.Notification.NotificationType;

public class IncludeBatchTest {

    private static final String highlightFormat = "[b]%s[/b]";
    private static final String missingParameterFormat = String.format(highlightFormat,
           RegularExpression.PARAMETER_PLACEHOLDER_ENCLOSING + "%s" + RegularExpression.PARAMETER_PLACEHOLDER_ENCLOSING);

    @Test
	public void testSimplePage() {
        IncludeBatch batch = createIncludeBatchWithTestParameter();
        String result = batch.insertIn("Testtext $param$ $param$$param$");
        assertEquals("Testtext value valuevalue", result);
        assertZeroError(batch);
	}

	@Test
	public void testComplicatedPage() {
        IncludeBatch batch = createIncludeBatchWithTestParameter();
        String result = batch.insertIn("Testtext test$test $ param$ $$param$ $param$$param$am$");
        assertEquals("Testtext test$test $ param$ $value valuevalueam$", result);
        assertZeroError(batch);
	}

	@Test
	public void testMultipleParameter() {
	    IncludeBatch batch = createIncludeBatchWithMultipleTestParameters(3);
        String result = batch.insertIn("Testtext $param$text$param2$ $param3$$param2$");
        assertEquals("Testtext valuetextvalue2 value3value2", result);
        assertZeroError(batch);
	}

	@Test
	public void testUnusedParameter() {
        IncludeBatch batch = createIncludeBatchWithTestParameter();
        String result = batch.insertIn("No parameter at all");
        assertEquals("No parameter at all", result);
        assertError(NotificationType.UNUSED_PARAMETER, batch);
	}

    @Test
	public void testMissingParameter() {
        IncludeBatch batch = createIncludeBatchWithTestParameter();
        String result = batch.insertIn("Existing: $param$ Nonexistent: $para$");
        assertEquals("Existing: value Nonexistent: " + String.format(missingParameterFormat, "para"), result);
		assertError(NotificationType.MISSING_PARAMETER, batch);
	}

    @Test
    public void testMissingAndUnusedParameter() {
        IncludeBatch batch = createIncludeBatchWithTestParameter();
        String result = batch.insertIn("Nonexistent: $para$");
        assertEquals("Nonexistent: " + String.format(missingParameterFormat, "para"), result);
        assertError(NotificationType.MISSING_PARAMETER, batch);
        assertError(NotificationType.UNUSED_PARAMETER, batch);
    }

    @Test
    public void testHtmlEcapedValue() {
        Map<String, ParameterDeclaration> parameters = new HashMap<String, ParameterDeclaration>();
        parameters.put("param", ParameterDeclaration.create("param", "<p>value</p>"));
        IncludeBatch batch = createIncludeBatch(parameters);
        String result = batch.insertIn("Escaped: $param$");
        assertEquals("Escaped: &lt;p&gt;value&lt;/p&gt;", result);
        assertZeroError(batch);
    }

    @Test
    public void testInheritedParameters() {
        IncludeBatch batch = createIncludeBatchWithInheritedParameter();
        String result = batch.insertIn("First depth: $param$ Second depth: $param2$");
        assertEquals("First depth: value Second depth: value2", result);
        assertZeroError(batch);
    }

    private IncludeBatch createIncludeBatchWithTestParameter() {
        return createIncludeBatchWithMultipleTestParameters(1);
    }

    private IncludeBatch createIncludeBatchWithMultipleTestParameters(int parameterCount) {
        return createIncludeBatch(getReferenceIncludeParameters(parameterCount));
    }

    private IncludeBatch createIncludeBatch(Map<String, ParameterDeclaration> parameters) {
        Stack<Map<String, ParameterDeclaration>> inheritanceStack = new Stack<Map<String,ParameterDeclaration>>();
        return new IncludeBatch(parameters, inheritanceStack, highlightFormat);
    }

    private IncludeBatch createIncludeBatchWithInheritedParameter() {
        Map<String, ParameterDeclaration> parameters = Collections.emptyMap();
        Stack<Map<String, ParameterDeclaration>> inheritanceStack = new Stack<Map<String,ParameterDeclaration>>();
        inheritanceStack.push(getReferenceIncludeParameters(1));
        inheritanceStack.push(getReferenceIncludeParameters(2));
        inheritanceStack.push(getReferenceIncludeParameters(1));
        return new IncludeBatch(parameters, inheritanceStack, highlightFormat);
    }

    private Map<String, ParameterDeclaration> getReferenceIncludeParameters(int parameterCount) {
        Map<String, ParameterDeclaration> parameters = new HashMap<String, ParameterDeclaration>();
        if (parameterCount >= 1) {
            parameters.put("param", ParameterDeclaration.create("name", "value"));
            while (parameters.size() < parameterCount) {
                String name = "param" + (parameters.size() + 1);
                ParameterDeclaration parameter = ParameterDeclaration.create(name, "value" + (parameters.size() + 1));
                parameters.put(name, parameter);
            }
        }
        return parameters;
    }

    private static void assertZeroError(IncludeBatch batch) {
        assertTrue("Unexpected errors occured", batch.getNotifications().isEmpty());
    }

    private static void assertError(NotificationType type, IncludeBatch batch) {
        for (Notification error : batch.getNotifications()) {
            if (error.getType().equals(type)) {
                return;
            }
        }
        fail("Expected error with type " + type);
    }
}
