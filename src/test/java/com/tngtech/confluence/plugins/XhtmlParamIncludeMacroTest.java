package com.tngtech.confluence.plugins;

import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.tngtech.confluence.plugins.MockFactory.PageMock;
import com.tngtech.confluence.plugins.view.Message.MessageType;

public class XhtmlParamIncludeMacroTest {

    private XhtmlParamIncludeMacro xhtmlMacroUnderTest;

    @Before
    public void setUp() {
        PageManager pageManager = MockFactory.mockPageManager();
        PermissionManager permissionManager = MockFactory.mockPermissionManager();
        XhtmlContent xhtmlUtils = XhtmlMockFactory.mockXhtmlContent();
        I18N i18n = MockFactory.mockI18N();
        xhtmlMacroUnderTest = new XhtmlParamIncludeMacro(pageManager, permissionManager, xhtmlUtils, i18n);
    }

    @Test
    public void testValidInclude() {
        String result = executeMacro(PageMock.EXISTING, "");
        assertZeroMessage(result);
    }

    @Test
    public void testMissingPage() {
        String result = executeMacro(PageMock.MISSING, "");
        assertMessage(MessageType.WARNING, result);
        assertContains(getI18NMessageTitle(I18N.MISSING_PAGE), result);
    }

    @Test
    public void testInsufficientPermissions() {
        String result = executeMacro(PageMock.FORBIDDEN, "");
        assertMessage(MessageType.WARNING, result);
        assertContains(getI18NMessageTitle(I18N.INSUFFICIENT_PERMISSIONS), result);
    }

    @Test
    public void testParseError() {
        String result = executeMacro(PageMock.EXISTING, "invalid");
        assertMessage(MessageType.WARNING, result);
        assertContains(getI18NMessageTitle(I18N.PARSE_ERROR), result);
    }

    @Test
    public void testUnusedParameter() {
        String result = executeMacro(PageMock.EXISTING, "unused=value");
        assertMessage(MessageType.NOTE, result);
        assertContains(getI18NMessageTitle(I18N.UNUSED_PARAMETER), result);
    }

    @Test
    public void testMissingParameter() {
        String result = executeMacro(PageMock.ONEPARAM, "");
        assertMessage(MessageType.WARNING, result);
        assertContains(getI18NMessageTitle(I18N.MISSING_PARAMETER), result);
    }

    @Test
    public void testMissingIdent() {
        Map<String, String> parameters = Collections.emptyMap();
        String result = executeMacro(parameters, "");
        assertMessage(MessageType.WARNING, result);
        assertContains(getI18NMessageTitle(I18N.MISSING_IDENT), result);
    }

    @Test
    public void testEmptyIdent() {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("page", "");
        String result = executeMacro(parameters, "");
        assertMessage(MessageType.WARNING, result);
        assertContains(getI18NMessageTitle(I18N.MISSING_IDENT), result);
    }

    @Test
    public void testBlogIncluded() {
        String result = executeMacro(PageMock.BLOG, "");
        assertMessage(MessageType.WARNING, result);
        assertContains(getI18NMessageTitle(I18N.INCLUDED_BLOG), result);
    }

    @Test
    public void testRecursionDetected() {
        String result = executeMacro(PageMock.RECURSION, "");
        assertMessage(MessageType.WARNING, result);
        assertContains(getI18NMessageTitle(I18N.INLCUDE_RECURSION), result);
    }

    private String executeMacro(PageMock page, String macroBody) {
        Map<String, String> macroParameters = new HashMap<String, String>();
        macroParameters.put("page", page.toString());
        return executeMacro(macroParameters, macroBody);
    }

    private String executeMacro(Map<String, String> macroParameters, String macroBody) {
        ConversionContext context = XhtmlMockFactory.mockConversionContext();
        return xhtmlMacroUnderTest.execute(macroParameters, macroBody, context);
    }

    private static void assertZeroMessage(String result) {
        for (MessageType type : MessageType.values()) {
            assertTrue("Unexpected: [" + type + "] in: [" + result + ']', !result.contains(type.toString()));
        }
    }

    private static void assertMessage(MessageType type, String result) {
        assertContains(type.toString(), result);
    }

    private static void assertContains(String subsequence, String result) {
        assertTrue("Expected: [" + subsequence + "] in: [" + result + ']', result.contains(subsequence));
    }

    private static String getI18NMessageTitle(String key) {
        return MockFactory.mockI18N().getMessageTitle(key);
    }
}