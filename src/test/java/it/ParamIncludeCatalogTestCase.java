package it;

import java.util.List;
import java.util.regex.Pattern;

import net.sourceforge.jwebunit.api.IElement;

import org.junit.Test;


public class ParamIncludeCatalogTestCase extends AbstractIntegrationTestCase {

    @Test
    public void testCatalog() {
        visitPageAsUser("CatalogTest", "it");
        assertMessageCount(12);
        assertMessageMatch(MessageType.INFO, "CatalogPage.*(it)");
        assertMessageMatch(MessageType.INFO, "Description");
        assertMessageMatch(MessageType.INFO, Pattern.quote("Some random description text."));
        assertPanelMatch(Pattern.quote("A simple page with a description in it."));
    }

    @Test
    public void testCatalogRecursionProtection() {
        visitPageAsUser("CatalogTest", "it");
        assertMessageMatch(MessageType.NOTE, createIncludedCatalogPattern("CatalogTest"));
    }

    private String createIncludedCatalogPattern(String title) {
        return Pattern.quote("Infinite loop prevented") + ".*" + Pattern.quote("The page \"" + title + "\" contains a \"Param Include Catalog\" macro and is labeled as include page. " +
        		"This would lead to an infinite loop when displaying the catalog. Please remove label \"include\" from page \"" + title + "\".");
    }

    @Test
    public void testTwoDescriptionDisplay() {
        visitPageAsUser("TwoDescriptionTest", "it");
        assertMessageCount(2);
        assertMessageMatch(MessageType.INFO, Pattern.quote("Description 1 of 2"));
        assertMessageMatch(MessageType.INFO, Pattern.quote("Description 2 of 2"));
    }

    @Test
    public void testTwoDescriptionFiltering() {
        visitPageAsUser("CatalogTest", "it");
        assertMessageMatch(MessageType.INFO, Pattern.quote("Description 1 of 2"));
        assertMessageNotMatch(MessageType.INFO, Pattern.quote("Description 2 of 2"));
    }

    @Test
    public void testNestedPanel() {
        visitPageAsUser("CatalogTest", "it");
        assertPanelMatch(Pattern.quote("Some normal text."));
        assertPanelMatch(Pattern.quote("This causes problems in Confluence 3.5 as Catalog uses panel to display pages and macro can't be nested."));
        assertPanelMatch(Pattern.quote("Some more text."));
        // Checks if inner panel is present and rendered correct to HTML
        assertPanelHasChildren("descendant::*[@class='panelContent']");
    }

    private void assertMessageNotMatch(MessageType type, String regularExpression) {
        assertFalse("Unexpected Message with type: [" + type + "] and content matching: [" + regularExpression + ']'
                + joinMessages(), hasElementMatch(getMessages(type), regularExpression));
    }

    private void assertPanelMatch(String regularExpression) {
        assertTrue("Expected panel with content matching: [" + regularExpression + ']'
                + joinMessages(), hasElementMatch(getPanels(), regularExpression));
    }

    private void assertPanelHasChildren(String xpath) {
        assertTrue("Element not found.", hasChildren(getPanels(), xpath));
    }

    private List<IElement> getPanels() {
        return getMainDiv().getElements("descendant::*[@class='panelContent']");
    }
}
