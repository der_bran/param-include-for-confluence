package it;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;

import net.sourceforge.jwebunit.api.IElement;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.Ignore;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.JWebUnitConfluenceWebTester;
import com.atlassian.confluence.plugin.functest.TesterConfiguration;

@Ignore("Abstract class")
public class AbstractIntegrationTestCase extends AbstractConfluencePluginWebTestCase {

    protected static final String testSpace = "it";

    protected static final String testUser = "test";
    protected static final String testPassword = "test";

    private boolean localeManipulated = false;

    @Override
    protected JWebUnitConfluenceWebTester createConfluenceWebTester()
    {
        Properties properties = new Properties();
        properties.put("confluence.webapp.protocol", "http");
        properties.put("confluence.webapp.host", "localhost");
        properties.put("confluence.webapp.port", Integer.parseInt(System.getProperty("http.port")));
        properties.put("confluence.webapp.context.path", System.getProperty("context.path"));
        properties.put("confluence.auth.admin.username", "admin");
        properties.put("confluence.auth.admin.password", "admin");

        TesterConfiguration configuration;
        try {
            configuration = new TesterConfiguration(properties);
        } catch (IOException e) {
            fail("Unable to create tester: " + e.getMessage());
            return null;
        }

        JWebUnitConfluenceWebTester tester = new JWebUnitConfluenceWebTester(configuration);
        tester.getTestContext().setBaseUrl(tester.getBaseUrl());
        tester.setScriptingEnabled(false);
        return tester;
    }

    @Override
    protected void setUp() throws Exception {
        Logger rootLogger = Logger.getRootLogger();
        rootLogger.setLevel(Level.INFO);
        rootLogger.addAppender(new ConsoleAppender( new PatternLayout("%-6r [%p] %c - %m%n")));
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        if (localeManipulated) {
            changeUserLocale(UserLocale.DEFAULT); // Ensures that Locale is reseted, even if tests fail.
        }
        super.tearDown();
    }

    protected void visitPageAsUser(String title, String key) {
        startSession();
        visitPage(title, key);
    }

    protected void startSession() {
        beginAt("");
        login(testUser, testPassword);
    }

    protected void visitPage(String title, String key) {
        gotoPage("display/" + key + '/' + title.replace(" ", "%20"));
        assertTitleMatch(".*" + title + ".*");
    }

    protected enum UserLocale {
        DEFAULT("None"),
        GERMAN("de_DE"),
        ENGLISH("en_GB"),
        FRENCH("fr_FR");
        private String locale;
        private UserLocale (String locale) {
            this.locale = locale;
        }
        @Override
        public String toString() {
            return locale;
        }
    }

    // Workaround cause language selection with HTTP-header wasn't reliable
    protected void changeUserLocale(UserLocale locale) {
        localeManipulated = true;
        gotoPage("/users/viewmysettings.action");
        clickButton("edit");
        selectOptionByValue("preferredUserLocale", locale.toString());
        submit();
    }

    protected void assertParagraph(String expected, int position) {
        assertEquals(expected, getParagraph(position).getTextContent());
    }

    protected void assertParagraphHasChildren(String xpath, int position) {
        // getElement(...) throws NullPointerException instead of returning null as stated in the javadoc
        //  -> Solution: use getElements(...) and check list.
        assertFalse("Element not found.", getParagraph(position).getElements(xpath).isEmpty());
    }

    protected void assertParagraphHasNotChildren(String xpath, int position) {
        // Please regard comment in assertParagraphHasChildren(...)
        assertTrue("Element found.", getParagraph(position).getElements(xpath).isEmpty());
    }

    protected IElement getParagraph(int position) {
        return getMainDiv().getElement("p[" + (position + 1) + ']');
    }

    public enum MessageType {
        INFO("infoMacro"), NOTE("noteMacro"), WARNING("warningMacro");

        private String macroName;

        private MessageType(String macroName) {
            this.macroName = macroName;
        }

        @Override
        public String toString() {
            return macroName;
        }
    }

    protected void assertMessageMatch(MessageType type, String regularExpression) {
        assertTrue("Expected message with type: [" + type + "] and content matching: [" + regularExpression + ']'
                + joinMessages(), hasElementMatch(getMessages(type), regularExpression));
    }

    protected boolean hasElementMatch(List<IElement> elements, String regularExpression) {
        Pattern pattern = Pattern.compile(regularExpression, Pattern.DOTALL);
        for (IElement element : elements) {
            if (pattern.matcher(element.getTextContent()).find()) {
                return true;
            }
        }
        return false;
    }

    protected String joinMessages() {
        List<IElement> elements = getMessages();
        if (elements.isEmpty()) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        builder.append("\nPresent Messages:\n");
        for (IElement element : elements) {
            builder.append("Type: [").append(element.getAttribute("class"))
                    .append("] With Content Matching: [").append(element.getTextContent())
                    .append("]\n");
        }
        return builder.toString();
    }

    protected void assertMessageCount(int expected) {
        List<IElement> messages = getMessages();
        for (IElement e : messages) {
            System.out.println(e);
        }
        assertEquals("Wrong count of messages.", expected, getMessages().size());
    }

    private List<IElement> getMessages() {
        return getMainDiv().getElements("descendant::*[@class='warningMacro' or @class='noteMacro' or @class='infoMacro']");
    }

    protected List<IElement> getMessages(MessageType type) {
        return getMainDiv().getElements("descendant::*[@class='" + type + "']");
    }

    protected boolean hasChildren(List<IElement> elements, String xpath) {
        for (IElement element : elements) {
            if (!element.getElements(xpath).isEmpty()) {
                return true;
            }
        }
        return false;
    }

    protected IElement getMainDiv() {
        String xhtmlXPath = "descendant::div[@id='main-content']";
        String markupXPath = "descendant::div[@class='wiki-content']";
        return getElementByXPath(xhtmlXPath + "|" + markupXPath);
    }
}
