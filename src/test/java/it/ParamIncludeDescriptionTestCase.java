package it;

import java.util.regex.Pattern;

import org.junit.Test;


public class ParamIncludeDescriptionTestCase extends AbstractIntegrationTestCase {

    @Test
    public void testDescription() {
        visitPageAsUser("DescriptionTest", "it");
        assertMessageCount(1);
        assertMessageMatch(MessageType.INFO, "Description");
        assertMessageMatch(MessageType.INFO, Pattern.quote("This text is part of the description."));
        assertMessageMatch(MessageType.INFO, "Heading");
        assertMessageMatch(MessageType.INFO, Pattern.quote("Name of the person to greet."));
        assertMessageMatch(MessageType.INFO, Pattern.quote("Text to include to the page."));
        assertMessageMatch(MessageType.INFO, "Some.*formated.*text.");
    }

    @Test
    public void testDescriptionInclude() {
        visitPageAsUser("DescriptionIncludeTest", "it");
        assertParagraph("This text is part of the included page.", 0);
        assertMessageCount(0); // Description was filtered successfully
    }

    @Test
    public void testNestedMacro() {
        visitPageAsUser("NestedMacroTest", "it");
        assertMessageMatch(MessageType.INFO, "Description");
        assertMessageMatch(MessageType.INFO, Pattern.quote("I like cheese!"));
    }

    @Test
    public void testNoteInDescription() {
        visitPageAsUser("NoteInDescriptionTest", "it");
        assertMessageCount(2); // Description and note are counted
        assertMessageMatch(MessageType.INFO, "Description");
        assertMessageMatch(MessageType.INFO, Pattern.quote("Some text..."));
        assertMessageHasChildren(MessageType.INFO, "descendant::*[@class='noteMacro']");
        assertMessageMatch(MessageType.INFO, Pattern.quote("Some more text..."));
        assertParagraph("The description is tested here.", 0);
    }

    private void assertMessageHasChildren(MessageType type, String xpath) {
        assertTrue("Element not found.", hasChildren(getMessages(type), xpath));
    }
}
