package it;

import java.util.List;
import java.util.regex.Pattern;

import net.sourceforge.jwebunit.api.IElement;

import org.junit.Test;

import com.tngtech.confluence.plugins.process.RegularExpression;
import com.tngtech.confluence.plugins.process.StringHelper;


public class ParamIncludeTestCase extends AbstractIntegrationTestCase {

    @Test
    public void testNonExistingPage() {
        visitPageAsUser("NonExistingPageTest", "it");
        assertMessageCount(1);
        assertMessageMatch(MessageType.WARNING, createMissingPagePattern("NonExistingPage"));
    }

    private String createMissingPagePattern(String title) {
        return Pattern.quote("Page not found") + ".*" + Pattern.quote("The include page \"" + title + "\" could not be retrieved.");
    }

    @Test
    public void testForbiddenPage() {
        visitPageAsUser("ForbiddenPageTest", "it");
        assertMessageCount(1);
        assertMessageMatch(MessageType.WARNING, createForbiddenPagePattern());
    }

    private String createForbiddenPagePattern() {
        return Pattern.quote("Insufficient permissions") + ".*" + Pattern.quote("You do not have sufficient permissions to view the included page.");
    }

    @Test
    public void testBlogPost() {
        visitPageAsUser("BlogPostTest", "it");
        assertMessageCount(1);
        assertMessageMatch(MessageType.WARNING, createIncludedBlogPattern("BlogPost"));
    }

    private String createIncludedBlogPattern(String title) {
        return Pattern.quote("Included blog post") + ".*" + Pattern.quote("The included page \"" + title
                + "\" is a blog post. Including of blog posts is not supported.");
    }

    @Test
    public void testParseError() {
        visitPageAsUser("ParseErrorTest", "it");
        assertMessageCount(2);
        assertMessageMatch(MessageType.WARNING, createParseErrorPattern("misformed statement"));
        assertMessageMatch(MessageType.WARNING, createMissingParameterPattern("name, text"));
        assertHighlightedParameterCount(2);
        assertHighlightedParameter("name");
        assertHighlightedParameter("text");
    }

    private String createParseErrorPattern(String statement) {
        return Pattern.quote("Invalid parameter") + ".*" + Pattern.quote("The declaration of a parameter could not be recognized. Parameters must be declared in the form \"name1=value1|name2=value2\"")
                + ".*" + Pattern.quote("Invalid parameter:") + ".*" + Pattern.quote(statement);
    }

    private String createMissingParameterPattern(String parameters) {
        return Pattern.quote("Missing parameter") + ".*" + Pattern.quote("For some place holder in the include page no parameter was declared.")
                + ".*" + Pattern.quote("Missing parameter:") + ".*" + Pattern.quote(parameters);
    }

    @Test
    public void testTwoParameterMissing() {
        visitPageAsUser("TwoParameterMissingTest", "it");
        assertMessageCount(1);
        assertMessageMatch(MessageType.WARNING, createMissingParameterPattern("name, text"));
        assertHighlightedParameterCount(2);
        assertHighlightedParameter("name");
        assertHighlightedParameter("text");
    }

    @Test
    public void testOneParameterMissing() {
        visitPageAsUser("OneParameterMissingTest", "it");
        assertMessageCount(1);
        assertMessageMatch(MessageType.WARNING, createMissingParameterPattern("name"));
        assertHighlightedParameterCount(1);
        assertHighlightedParameter("name");
    }

    @Test
    public void testUnusedParameter() {
        visitPageAsUser("UnusedParameterTest", "it");
        assertMessageCount(1);
        assertMessageMatch(MessageType.NOTE, createUnusedParameterPattern("param"));
        assertHighlightedParameterCount(0);
    }

    private String createUnusedParameterPattern(String parameters) {
        return Pattern.quote("Unused parameter") + ".*" + Pattern.quote("Not all given parameters could be assigned to place holders in the include page.")
                + ".*" + Pattern.quote("Unused parameter:") + ".*" + Pattern.quote(parameters);
    }

    @Test
    public void testCorrectUsage() {
        visitPageAsUser("CorrectUsageTest", "it");
        assertParagraph("Hello user!", 0);
        assertParagraph("This is included text.", 1);
    }

    @Test
    public void testIncludeEscape() {
        visitPageAsUser("IncludeEscapeTest", "it");
        assertParagraph("1|param1=x", 0);
        assertParagraph("x", 1);
        assertParagraph("3\\\\\\\\|param3=x", 2);
    }

    @Test
    public void testPreformatedParameter() {
        visitPageAsUser("PreformatedParameterTest", "it");
        assertParagraph("This text should be bold.", 1);
        String xhtmlXPath = "strong";
        String markupXPath = "b";
        assertParagraphHasChildren(xhtmlXPath + "|" + markupXPath, 1);
    }

    @Test
    public void testXHtmlInjection() {
        visitPageAsUser("XHtmlInjectionTest", "it");
        assertMessageCount(0);
        assertParagraph("<ac:macro ac:name=\"info\"><ac:rich-text-body><p>This is my <strong>Description</strong>.</p></ac:rich-text-body></ac:macro>", 1);
        String xhtmlPattern = Pattern.quote("\\{info\\}test\\{info\\}");
        String markupPattern = Pattern.quote("{info}test{info}");
        assertParagraphMatch(xhtmlPattern + '|' + markupPattern, 3);
        assertParagraph("<strong>test</strong>", 5);
        String xhtmlXPath = "strong";
        String markupXPath = "b";
        assertParagraphHasNotChildren(xhtmlXPath + "|" + markupXPath, 5);
        assertParagraphHasChildren(xhtmlXPath + "|" + markupXPath, 6);
    }

    @Test
    public void testMessageInjection() {
        visitPageAsUser("MessageInjectionTest", "it");
        assertMessageCount(1);
        assertMessageMatch(MessageType.WARNING, createParseErrorPattern("</ac:rich-text-body></ac:macro>Poor try of injection into the error message."));
        assertParagraph("</body>", 1);
    }

    @Test
    public void testInternationalization() {
        visitPageAsUser("ParseErrorTest", "it");
        changeUserLocale(UserLocale.GERMAN);
        visitPage("ParseErrorTest", "it");
        assertMessageCount(2);
        assertMessageMatch(MessageType.WARNING, createGermanParseErrorPattern("misformed statement"));
        assertMessageMatch(MessageType.WARNING, createGermanMissingParameterPattern("name, text"));
    }

    private String createGermanParseErrorPattern(String statement) {
        return Pattern.quote("Unzulässige Parameter") + ".*" + Pattern.quote("Die Angabe eines Parameters wurde nicht erkannt. Parameter müssen in der Form \"name1=wert1|name2=wert2\" angegeben werden.")
                + ".*" + Pattern.quote("Unzulässige Parameter:") + ".*" + Pattern.quote(statement);
    }

    private String createGermanMissingParameterPattern(String parameters) {
        return Pattern.quote("Fehlende Parameter") + ".*"
                + Pattern.quote("Für einige Platzhalter in der einzubettenden Seite wurde kein Parameter angegeben.")
                + ".*" + Pattern.quote("Fehlende Parameter:") + ".*" + Pattern.quote(parameters);
    }

    @Test
    public void testDirectInclude() {
        visitPageAsUser("DirectIncludeTest", "it");
        assertMessageCount(1);
        assertMessageMatch(MessageType.WARNING, createIncludeRecursionPattern("DirectIncludeTest"));
    }

    private String createIncludeRecursionPattern(String title) {
        return Pattern.quote("Infinite loop prevented") + ".*" + Pattern.quote("The page \"" + title + "\" included itself direct or indirect by another page. This would lead to an infinite loop when displaying the page.");
    }

    @Test
    public void testIndirectInclude() {
        visitPageAsUser("IndirectIncludeTest", "it");
        assertMessageCount(1);
        assertMessageMatch(MessageType.WARNING, createIncludeRecursionPattern("IndirectIncludeFirstPage"));
        assertParagraph("This is the second page.", 0);
        assertParagraph("This is the first page.", 1);
    }

    @Test
    public void testParameterInheritance() {
        visitPageAsUser("ParameterInheritanceTest", "it");
        assertMessageCount(0);
        assertParagraph("Here the inner declaration should be used:", 0);
        assertParagraph("Inner declaration of parameter.", 1);
        // Next paragraph starts in confluence 3.5 with a zero width non breaking space, using regular match for this one.
        assertParagraphMatch(Pattern.quote("No inner declaration given, parameter should be inheritated from surrounding macro:"), 2);
        assertParagraph("Outer declaration of parameter.", 3);
        assertParagraph("Parameter isn't used here, but is still distinguished as used.", 4);
    }

    private void assertHighlightedParameter(String parameter) {
        String enclosing = RegularExpression.PARAMETER_PLACEHOLDER_ENCLOSING;
        for (IElement element : getHighLightedParameters()) {
            if (element.getTextContent().equals(enclosing + parameter + enclosing)) {
                return;
            }
        }
        String listing = StringHelper.join(", ", getHighLightedParameters());
        fail("Expected Parameter [" + parameter + "] In: [" + listing + ']');
    }

    private void assertHighlightedParameterCount(int expected) {
        assertEquals("Wrong count of highlighted parameters.", expected, getHighLightedParameters().size());
    }

    private List<IElement> getHighLightedParameters() {
        String xhtmlXPath = "descendant::span[@style='color: rgb(255,0,0);']/strong";
        String markupXPath = "descendant::font[@color='#ff0000']/b";
        return getMainDiv().getElements(xhtmlXPath + "|" + markupXPath);
    }

    protected void assertParagraphMatch(String regularExpression, int position) {
        Pattern pattern = Pattern.compile(regularExpression, Pattern.DOTALL);
        String content = getParagraph(position).getTextContent();
        assertTrue("Expected paragraph content matching: [" + regularExpression + "] but was [" + content + "]",
                pattern.matcher(content).find());
    }
}
