
=== ABSTRACT ===

Nothing to do when building for Confluence 4.0 and above:
 e.g. "atlas-mvn [lifecycle]"

Activate Confluence 3.5 profile when building for Confluence 3.5:
 e.g. "atlas-mvn [lifecycle] -P confluence-3.5"

Caution:
With Atlassian SDK 3.10 on Windows "atlas-integration-test" will ignore profile.

=== README ====

This Confluence plugin is for use in Confluence 3.5 and Confluence 4.0. It is written native for Confluence 4.x. Since extensive changes took place from version 3.5 to 4.0 (details in the next section), the parts interacting with Confluence itself had to be rewritten for Confluence 3.5.

To achieve macro compatibility, two maven profiles are defined within the POM.xml. The profile "Confluence-4.x" is the default build profile. To build against Confluence 3.5 you have to trigger the associated profile "confluence-3.5". Use e.g. "atlas-package -P confluence-3.5" to enable the maven profile and building the package for Confluence 3.5.

This source comes with integration test databases for both Confluence version. The integration tests themselves are version independent.

You can use maven goals to generate IDE-files e.g. "atlas-mvn eclipse:eclipse". Just add the profile parameter if needed. Atlas scripts in the atlassian plugin SDK should work with the profile parameter, too.

Caution:
With Atlassian SDK 3.10 on Windows, the command "atlas-integration-test" will ignore the profile. Use "atlas-mvn integration-test -P confluence-3.5" instead. Other commands will work fine under Windows, too.

=== REASONS ===

This project consists of three source folders: version independent source (which implements the basic functionality), and Macro- and Helper-classes each for Confluence 3.5 and Confluence 4.0 and above. Test source and resources are splitted accordingly.

Reasons for this mess:
1. Some methods exists in Confluence 3.5, but don't exists in Confluence 4.x and vice versa. Without usage of tools like reflection, the source couldn't be build against neither version of Confluence. HelperClasses which are injected in the version independant base source are needed.

2. The interface respectively the abstract base class of Macros has changed. Parameter classes like the context has changed either.

3. The internal representation of data has changed from markup to xhtml based storage format. The output uses internal Confluence macros (like the warning or info boxes) to maintain the user configured look and feel within Param Include. The rendering and representation of these macros has changed from markup to xhtml, too.

